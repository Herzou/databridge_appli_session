<?php defined('BASEPATH') OR exit('No direct script access allowed');


class User_login_mod extends MY_Model
{
    private $USERNAME = null; //nom d'utilisateur
    private $MATRICULE = null; //num matricule
    private $DIR_NAME = null; //bureau fiscal
    private $OPER_TYPE = null;
    private $OPER_TK = null;
    private $DIR_CODE = null; //code bureau de l'utilisateur
    private $KEY = null;
    private $VALID_CDBUR = null; //code bureau validateur
    private $VALID_BUREAU = null;  //Libellé bureau validateur
    private $USER_CODE = null;
    private $EMAIL = null;
    private $USER_ERROR = null;
    private $LABEL = null; //Type bureau validateur
    public $statut = false;
    protected $today; //date du jour
    protected $date;


    protected $table = 'GRH_USERS';

    public function __construct()
    {
        parent::__construct();

        $this->today = date('d/m/Y H:i:s');
        $this->date = date('Y-m-d H:i:s');
    }


    public function mydata()
    {

    }


    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @param string $table
     */
    public function resetTable()
    {
        $this->table = '';
    }
}