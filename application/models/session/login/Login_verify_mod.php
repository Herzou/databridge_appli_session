<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Login_verify_mod extends MY_Model
{
    private $message = '';
    private $error = 0;
    private $session_key = '';
    private $sessionExist = 0;
    private $sessionExpire = 0;
    private $sessionEnd = 0;
    private $username = '';
    private $email = '';

    protected $table = 'GRH_USERS';

    public function __construct(){

    }

    //verifier si l'utilisateur existe (type booléen)
    public function checkUser($pseudo) {
        $nbr = $this->count("USERNAME = '$pseudo' OR EMAIL = '$pseudo'");
        if ($nbr>= 1) {
            return true;
        } else {
            $this->setMessage('Nom d\'utilisateur invalide sur le système');
            $this->setError(1);
            return false;
        }
    }


    //verifier si le login et le mot de passe sont corrects (retourne un tableau de resultat);
    public function checkPass($user, $pass) {
        $passwd = hash('sha256', $pass);
        $nbr = $this->count("USERNAME = '$user' OR EMAIL = '$user' AND CD_ACCESS ='$passwd'");
        if ($nbr>=1) {
            $user = $this->readLine('USERNAME,EMAIL',"USERNAME = '$user' OR EMAIL = '$user' AND CD_ACCESS ='$passwd'");
            $this->setUsername($user->USERNAME);
            $this->setEmail($user->EMAIL);
            return true;
        } else {
            $this->setMessage('Mot de passe incorrect');
            $this->setError(1);
            return false;
        }
    }

    //Créer une session valide pour l'utilisateur connecté
    public function loginUser($user, $passwd,$lastId=null) {
        $check1 = $this->checkUser($user);
        if ($check1==true){
            $check2 = $this->checkPass($user,$passwd);
            if($check2==true){
                $this->isExpire($user);
                if ($this->sessionExpire==1){
                    $this->setSessionKey(generate_token(80));
                }else {
                    // code ...
                }

            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    //Verifier si une session utilisateur existe.
    public function existSession($pseudo)
    {
        $this->setTable('USER_SESSION');
        $nbr = $this->count("USERNAME = '$pseudo' OR EMAIL = '$pseudo'");
        $this->resetTable();
        if ($nbr>=1) {
            $this->setSessionExist(1);
            return true;
        } else {
            $this->setError(3);
            $this->setSessionExist(0);
            return false;
        }
    }



    //verifier si une session existante expire
    public function isExpire($pseudo)
    {
        $this->existSession($pseudo);
        if ($this->sessionExist==1){
            $this->setTable('USER_SESSION');
            $expire = $this->readLine('EXPIRE,FIN,ID_SESSION',"USERNAME = '$pseudo' OR EMAIL = '$pseudo'");
            $this->resetTable();
            $this->setSessionExpire($expire->EXPIRE);
            $this->setSessionEnd($expire->FIN);
            $this->setSessionKey($expire->ID_SESSION);
            return $expire->EXPIRE;
        }else {
            $this->setSessionExpire(1);
            return 1;
        }
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @return int
     */
    public function getSessionEnd()
    {
        return $this->sessionEnd;
    }

    /**
     * @param int $sessionEnd
     */
    public function setSessionEnd($sessionEnd)
    {
        $this->sessionEnd = $sessionEnd;
    }




    /**
     * @return bool
     */
    public function isSessionExist()
    {
        return $this->sessionExist;
    }

    /**
     * @param bool $sessionExist
     */
    public function setSessionExist($sessionExist)
    {
        $this->sessionExist = $sessionExist;
    }

    /**
     * @return int
     */
    public function getSessionExpire()
    {
        return $this->sessionExpire;
    }

    /**
     * @param int $sessionExpire
     */
    public function setSessionExpire($sessionExpire)
    {
        $this->sessionExpire = $sessionExpire;
    }





    /**
     * @return string
     */
    public function getSessionKey()
    {
        return $this->session_key;
    }

    /**
     * @param string $session_key
     */
    public function setSessionKey($session_key)
    {
        $this->session_key = $session_key;
    }




    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param int $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }



    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @param string $table
     */
    public function resetTable()
    {
        $this->table = 'GRH_USERS';
    }
}