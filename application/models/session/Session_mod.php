<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Session_mod extends MY_Model
{
    protected $table = 'USERS_INFOS';
    private $username='x';
    private $message ;
    private $mysession ;


    public function __construct()
    {
        parent::__construct();
    }

    //VERIFIER LA SESSION UTILISATEUR
    /*
     * Verifie si la sesseion utilisateur existe ou expiré.
     */
    public function getSession($token)
    {
        $this->setTable('USER_SESSION');
         $myData = $this->readLine('EXPIRE',array('ID_SESSION'=>$token));

        if ($myData && $myData->EXPIRE == 0)
         {
             $this->setUsername($token);
             $this->setMessage('Aucun');
             $this->setMysession(0);

         }elseif ($myData && $myData->EXPIRE == 1){
            $this->setMysession(1);
            $this->setMessage('Session Expiré');
        }else{
            $this->setMysession(2);
            $this->setMessage('Session invalide');
        }
    }

    /*
     * Recuperer le nom d'utilisateur d'une session en cours
     */
    public function setUsername($token)
    {
        $this->setTable('USER_SESSION');
        $myData = $this->readLine('USERNAME',array('ID_SESSION'=>$token));
        if($myData && $myData->USERNAME){
            $this->username = $myData->USERNAME;
        }
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }





    /**
     * @return string
     */
    public function getTable()
    {
        return $this -> table;
    }

    /**
     * @param string $SESSION
     */
    public function setTable($table)
    {
        $this -> table = $table;
    }

    /**
     * @param string $SESSION
     */
    public function resetTable()
    {
        $this -> table = 'USERS_INFOS';
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getMysession()
    {
        return $this->mysession;
    }

    /**
     * @param mixed $mysession
     */
    public function setMysession($mysession)
    {
        $this->mysession = $mysession;
    }


}