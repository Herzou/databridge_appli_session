<?php
/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 13/01/2021
 * Time: 12:24
 */

class Session_statut  extends MY_Model
{
    private $ID = null;
    private $MATRICULE = null;
    private $ID_SESSION = null;
    private $DEBUT = null;
    private $FIN = null;
    private $EXPIRE = null;
    private $USER_AGENT = null;
    private $IP_ADDR = null;
    private $EMAIL = null;
    private $USERNAME = null;
    private $EXIST = 0;
    private $STATUT = null;

    protected $table = 'USER_SESSION';



    public function _setParam($params){
        if (is_array($params) && !empty($params)){
            $sessions = $this->read(null, $params);
            if (isset($sessions[0])) {
                $this->setEXIST(1);
                $session = $sessions[0];
                foreach ($session as $item => $value) {
                    $this->$item = $value;
                }
                if ($this->EXPIRE == 1) {
                    $this->setSTATUT('Expiré');
                } else {
                    $this->setSTATUT('En cours');
                }
            }
        }
    }

    /**
     * @return null
     */
    public function getEXIST()
    {
        return $this->EXIST;
    }

    /**
     * @param null $EXIST
     */
    public function setEXIST($EXIST)
    {
        $this->EXIST = $EXIST;
    }



    /**
     * @return null
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param null $ID
     */
    public function setID($ID)
    {
        $this->ID = $ID;
    }

    /**
     * @return null
     */
    public function getMATRICULE()
    {
        return $this->MATRICULE;
    }

    /**
     * @param null $MATRICULE
     */
    public function setMATRICULE($MATRICULE)
    {
        $this->MATRICULE = $MATRICULE;
    }

    /**
     * @return null
     */
    public function getIDSESSION()
    {
        return $this->ID_SESSION;
    }

    /**
     * @param null $ID_SESSION
     */
    public function setIDSESSION($idSession)
    {
        $sessions = $this->read(null,array('ID_SESSION'=>$idSession));
        if(isset($sessions[0])){
            $this->setEXIST(1);
            $session = $sessions[0];
            foreach ($session as $item=>$value) {
                $this->$item = $value;
            }
            if ($this->EXPIRE==1){
                $this->setSTATUT('Expiré');
            }else{
                $this->setSTATUT('En cours');
            }
        }

    }

    /**
     * @return null
     */
    public function getDEBUT()
    {
        return $this->DEBUT;
    }

    /**
     * @param null $DEBUT
     */
    public function setDEBUT($DEBUT)
    {
        $this->DEBUT = $DEBUT;
    }

    /**
     * @return null
     */
    public function getFIN()
    {
        return $this->FIN;
    }

    /**
     * @param null $FIN
     */
    public function setFIN($FIN)
    {
        $this->FIN = $FIN;
    }

    /**
     * @return null
     */
    public function getEXPIRE()
    {
        return $this->EXPIRE;
    }

    /**
     * @param null $EXPIRE
     */
    public function setEXPIRE($EXPIRE)
    {
        $this->EXPIRE = $EXPIRE;
    }

    /**
     * @return null
     */
    public function getUSERAGENT()
    {
        return $this->USER_AGENT;
    }

    /**
     * @param null $USER_AGENT
     */
    public function setUSERAGENT($USER_AGENT)
    {
        $this->USER_AGENT = $USER_AGENT;
    }

    /**
     * @return null
     */
    public function getIPADDR()
    {
        return $this->IP_ADDR;
    }

    /**
     * @param null $IP_ADDR
     */
    public function setIPADDR($IP_ADDR)
    {
        $this->IP_ADDR = $IP_ADDR;
    }

    /**
     * @return null
     */
    public function getEMAIL()
    {
        return $this->EMAIL;
    }

    /**
     * @param null $EMAIL
     */
    public function setEMAIL($EMAIL)
    {
        $this->EMAIL = $EMAIL;
    }

    /**
     * @return null
     */
    public function getUSERNAME()
    {
        return $this->USERNAME;
    }

    /**
     * @param null $USERNAME
     */
    public function setUSERNAME($USERNAME)
    {
        $this->USERNAME = $USERNAME;
    }

    /**
     * @return null
     */
    public function getSTATUT()
    {
        return $this->STATUT;
    }

    /**
     * @param null $STATUT
     */
    public function setSTATUT($STATUT)
    {
        $this->STATUT = $STATUT;
    }




    /**
     * @return string
     */
    public function getTable()
    {
        return $this -> table;
    }

    /**
     * @param string $SESSION
     */
    public function setTable($table)
    {
        $this -> table = $table;
    }

    /**
     * @param string $SESSION
     */
    public function resetTable()
    {
        $this -> table = 'USER_SESSION';
    }


}