<?php defined('BASEPATH') OR exit('No direct script access allowed');


class App_mod extends MY_Model
{
    protected $table = 'USERAPPS';

    public function __construct()
    {
        parent::__construct();
    }

    //Details d'une ssession pour une application.
    public function sessionAppliInfo($user,$appli)
    {
       $appliInfos = $this->readLine(null,array('APP_NOM'=>$appli,'USERNAME'=>$user));
       return $appliInfos;
    }


    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @param string $table
     */
    public function resetTable()
    {
        $this->table = '';
    }
}