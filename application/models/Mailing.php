<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mailing extends CI_Model
{
    protected $data = array();
    protected $statut = false;
    protected $message = null;
    protected $mail_user = null;
    protected $mail_pass = null;


    public function __construct()
    {
        parent::__construct();
        $this->load->library('Grhdata', null, 'grhdb');
    }


    /**
     * @param $params
     * @param $message
     * @return bool
     */
    public function sendEmail($params, $message,$file_url=null)
    {
        // Configure email library
/*        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = $this->unlockSecure('bde)nlno_i<bk^eg,`kh');
        $config['smtp_pass'] = $this->unlockSecure('bdekmop\j/,,5');
        $config['mailtype'] = 'html';   */


        // Configure email library sendinblue
        $config['protocol'] = 'smtp';
        //$config['smtp_crypto'] = 'ssl';
        $config['smtp_host'] = 'smtp-relay.sendinblue.com';
        $config['smtp_port'] = 587;
        $config['smtp_user'] = 'herzou.net2job@gmail.com';
        $config['smtp_pass'] = 'mC09jwqgZcV3TMOB';
        $config['mailtype'] = 'html';

        //var_dump($config);
        //die();

        // Load email library and passing configured values to email library
        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");

        // Sender email address
        $this->email->from($params['from'], $params['name']);

        //Reply to
        $this->email->reply_to($params['replyto'], $params['name']);

        // Receiver email address
        $this->email->to($params['to']);

        //Copie of content separed by ","
        //$this->email->bcc($params['cc']);

        // Subject of email
        $this->email->subject($params['subject']);

        // Message in email
        $this->email->message($message);

        //fichier attaché
        if ($file_url!=null) {
            $this->email->attach($file_url);
        }



        if ($this->email->send()) {

            $this->message = 'Email envoyé avec succès';
            return true;

        } else {

            $this->message = 'Impossible de se connecter au serveur email ' . $config['smtp_host'];
            return false;

        }

        return $this->statut;
    }


    /**
     * @param $user_mail
     * @param $username
     * @param $tk
     * @return bool
     */
    public function activationEmail($ObjUser,$parametres)
    {
        $params = array(
            'from' => 'admin.portal@impots.mg',
            'name' => 'DGI-Admin-portal',
            'to' => $ObjUser->getEMAIL(),
            'subject' => 'Activation DGI-Portal',
            'replyto' => 'dgi.portal@gmail.com'
        );

        $motdepasse = $parametres['pass'];
        $url_act = $parametres['url'];
        $url = $url_act. $ObjUser->getUSERNAME() . '/' . $ObjUser->getACTTOKEN();

        $message  = 'Bonjour  ' . $ObjUser->getPRENOM() . ', <br><br>';
        $message .= 'Votre compte utilisateur sur DGI-Portal a été crée avec succès. Voici quelques details à retenir concernant votre nouveau compte:<br><br>';
        $message .= 'Nom et Prenom : ' . $ObjUser->getNOM() . ' '.$ObjUser->getPRENOM(). '<br>';
        $message .= 'Email : ' . $ObjUser->getEMAIL() . '<br>';
        $message .= 'Téléphone : ' . $ObjUser->getTEL(). '<br><br><br>';
        $message .= 'Nom d\'utilisateur : ' . $ObjUser->getUSERNAME() . '<br>';
        $message .= 'Mot de passe       : ' . $motdepasse . '<br>';

        $message .= '<br>Cliquer sur le lien suivant pour activer votre nouveau compte maintenant. <br>';
        $message .= anchor($url, 'CLIQUER ICI POUR ACTIVER VOTRE COMPTE MAINTENANT', array('target' => '_blank', 'class' => 'link')) . '<br><br>';

        $message .= '<br>Si le lien donné au dessus ne foctionne pas. Copier et coller le lien texte suivant dans la barre d\'adresse de votre navigateur internet et ensuite valider. <br>';
        $message .= '<br>' .$url. '<br>';
        $message .= '<br>Merci pour votre inscription !<br><br>';

        $message .= 'Cordialement <br>';
        $message .= 'Admin DGI - PORTAL<br>';
        $message .= 'dgi.portal@impots.mg<br>';

        $send = $this->sendEmail($params, $message);
        return $send;
    }


    /**
     * @param $user_mail
     * @param $username
     * @param $reset_tk
     * @return bool
     */
    public function passwordEmail($user)
    {
        $params = array(
            'from' => 'dgi.portal@gmail.com',
            'name' => 'DGI-Portal',
            'to' => $user->getEMAIL(),
            'subject' => 'NOUVEAU MOT DE PASSE',
            'replyto' => 'dgi.portal@gmail.com'
        );

        $url = PORTAL_LINK. 'reset/resetpass/' .$user->getUSERNAME().'/'. $user->getRESTOKEN();
        $message = 'Bonjour  ' . $user->getNOM() . ' ' .$user->getPRENOM().', <br>';
        $message .= 'Cliquer sur le lien suivant pour enregister un nouveau  mot de passe à votre votre compte DGI-Portal :<br><br>';
        $message .= anchor($url, 'CHANGER MON MOT  DE PASSE', array('target' => '_blank', 'class' => 'link')) . '<br><br>';

        $message .= '<br>Si le lien donné au dessus ne foctionne pas. Copier et coller le lien texte suivant dans la barre d\'adresse de votre navigateur internet et ensuite valider. <br>';
        $message .= anchor($url, $url, array('target' => '_blank', 'class' => 'link')) . '<br><br>';


        $message .= '<br>Merci pour votre inscription !<br><br>';
        $message .= 'Cordialement <br>';
        $message .= 'Admin - DGI-Portal. <br>';



        $send = $this->sendEmail($params, $message);

        return $send;
    }

    /**
     * @param $user_mail
     * @param $username
     * @param $reset_tk
     * @return bool
     */
    public function demadeEmail($user,$apps,$detail=null)
    {
        $adminlist = implode(',',$this->adminList());
        $params = array(
            'from' => 'dgi.portal@gmail.com',
            'name' => '/'.$user->getUSERNAME().' / Dgi-Portal',
            'to' => $adminlist,
            'subject' => 'Inscription de '.$user->getUSERNAME(),
            'replyto' => $user->getEMAIL()
        );

        $message = 'Bonjour, <br><br>';
        $message .= 'L\'utilisateur '.$user->getNOM().' '. $user->getPRENOM().' vient de créer un noveau compte DGI-Portal dont voici les details : <br><br>';

        $message .= 'Nom d\'utilisateur : '.$user->getUSERNAME().'<br>';
        $message .= 'Email              : '.$user->getEMAIL().'<br>';
        $message .= 'Téléphone          : '.$user->getTEL().'<br>';
        $message .= 'Etablissement      : '.$user->getETABLISSEMENT().'<br>';
        $message .= 'Direction          : '.$user->getDIRECTION().'<br>';
        $message .= 'Service            : '.$user->getSERVICE().'<br>';
        $message .= 'Fonction           : '.$user->getFONCTION().'<br><br>';

        if (!empty($apps)){
            $message .= 'Il(Elle) vous demande l\'accès aux applications suivantes: <br>';
            foreach($apps as $key => $app){
                $message .=  '    - '.$app.' <br>';
            }
        }else{
            $message .= 'Il(Elle) vous demande la modification des accès aux applications attribuées par défaut <br>';
        }


        if ($detail!=null && $detail!=''){
            $message .= '<br>Dont voici des details supplémentaires à ajouter <br>';
            $message .=  '"'.$detail.'"<br>';
        }

        $message .= '<br>Merci !<br><br>';

        $message .= 'Cordialement <br>';

        $message .= 'Admin - DGI-Portal. <br>';

        $file = FILES.$user->getFICID();

        $send = $this->sendEmail($params,$message,$file);

        return $send;
    }


    public function validationEmail($datas)
    {
        $params = array(
            'from' => 'dgi.portal@gmail.com',
            'name' => 'Dgi-Portal - Admin',
            'to' => $datas['email'],
            'subject' => 'VALIDATION DE VOTRE COMPTE DGI-PORTAL',
            'replyto' => 'dgi.portal@gmail.com'
        );

        $lists = $datas['apps'];

        $message = 'Bonjour, <br><br>';
        $message .= 'Votre demande d\'accès aux applications dans votre compte DGI-Portal est actuellement approuvé par l\'administrateur. <br><br>';

        $message .= 'Voici la nouvelle liste des applications disponibles depuis votre espace utilisateur DGI-Portal: <br>';
        $message .= '<ul>';
        foreach ($lists as $key=>$list) {
            $message .= '<li>'.$key.' ---> '.$list.'</li>';
        }
        $message .='</ul>';

        $message .='Cliquer sur le lien suivant pour se connecter à votre Compte DGI-portal <br><a href="http://portal.impots.mg">http://portal.impots.mg</a>';

        $message .= '<br>Cordialement <br>';

        $message .= 'Admin - DGI-Portal. <br>';

        $send = $this->sendEmail($params,$message);

        return $send;
    }


    /**
     * Envoyer un email pour contacter l'admin
     * @param $datas
     */
    public function contactAdmin($datas)
    {
        $adminlist = implode(',',$this->adminList());
        $from = $datas['email'];
        $user = $datas['user'];
        $sujet = $datas['sujet'];
        $params = array(
            'from' => $from,
            'name' => $user,
            'to' => $adminlist,
            'subject' => $sujet,
            'replyto' => $from
        );

        $message = $datas['message'];
        $message.= '<br><br><br>==============================<br>';
        $message.= 'Message envoyé par : '.$user.'<br>';
        $message.= 'Email : '.$from.'<br>';


        $send = $this->sendEmail($params,$message);

        return $send;
    }


    public function unlockSecure($string)
    {
        $result = '';
        $offset = 2;
        $length = 5;

        $chars = str_split(trim($string,' '));
        foreach ($chars as $value) {
            $result = $result.(chr(ord($value) + $offset));
            if ($offset<$length) {
                $offset ++;
            }else{
                $offset = 2;
            }
        }

        return $result;
    }


    public function adminList()
    {
        $lists = $this->grhdb->adminliste();
        foreach($lists as $admin){
           $liste[] = $admin->EMAIL;
        }
        return $liste;
    }

    /**
     * @return boolean
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param boolean $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param null $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param string $mail_user
     */
    public function setMailUser($mail_user)
    {
        $this->mail_user = $mail_user;
    }

    /**
     * @param string $mail_pass
     */
    public function setMailPass($mail_pass)
    {
        $this->mail_pass = $mail_pass;
    }




}
