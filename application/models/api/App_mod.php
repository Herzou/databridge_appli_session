<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_mod extends MY_Model
{

    private $errors = array();
    protected $table = 'APP_LIST';


    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @return mixed
     */
    public function ApplicationsList()
    {
        return $this->read();
    }


    /**
     * @param null $id
     * @return bool
     */
    public function ApplicationDelete($id = null)
    {

        if ($id != null && is_int($id)) {
            $code_app = $this->getCodeApp($id);

            if ($code_app != null) {
                //TRANSACTION
                $this->openTrans();
                $this->delete(array('ID' => $id));
                $this->setTable('USER_APP');
                $this->delete(array('CD_APP' => $code_app));
                $this->setTable('APP_PRIVILEGE');
                $this->delete(array('CD_APP' => $code_app));
                $this->complete();
                $this->resetTable();
                //FIN TRANSACTION

                return $this->status();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }



    /**
     * @param $datas
     */
    public function ApplicationAdd($datas)
    {
        $code = (int)$this->maximum($field=null,$alias=null);
        $cd_app = $code+1;
        if (!empty($datas)) {
            $params = array(
                'APP_NOM' => $datas['appnom'],
                'DESCRIPTION' => $datas['desc'],
                'SLUG' => $datas['slug'],
                'STATUS' => $datas['statut'],
                'LOGO' => $datas['logo'],
                'URL' => $datas['url'],
                'CD_APP' => $cd_app,
                'DEFAULT_APP' => $datas['default'],
                'DEFAULT_PRIV' =>$datas['user']
            );
            $this->openTrans();
            $this->create($params);
            $this->complete();

            return $this->status();
        } else {
            return false;
        }
    }


    /**
     * @param $datas
     * @return bool
     */
    public function ApplicationUpdate($datas)
    {
        $updates = array();
        if (!empty($datas) && isset($datas['code'])) {
            $code = $datas['code'];
            $params = array(
                'APP_NOM' => $datas['appnom'],
                'DESCRIPTION' => $datas['desc'],
                'SLUG' => $datas['slug'],
                'STATUS' => $datas['statut'],
                'LOGO' => $datas['logo'],
                'URL' => $datas['url'],
                'DEFAULT_APP' => $datas['default'],
                'DEFAULT_PRIV' =>$datas['user']
            );


            foreach ($params as $key => $param) {
                if ($param != null && $param!='') {
                    $updates[$key] = $param;
                }
            }

            if ( count($updates)>=2) {
                $this->openTrans();
                $this->update($updates,'CD_APP='.$code);
                $this->complete();

                return $this->status();
            } else {
                return false;
            }

        } else {
            return false;
        }
    }



    /**
     * @param $code
     * @return mixed
     */
    public function ApplicationInfo($code)
    {
        return $this->readline(null,'CD_APP='.$code);
    }


    /**
     * @param $id
     * @return null
     */
    public function getCodeApp($id)
    {
        $data = $this->readLine('CD_APP', 'ID=' . $id);
        if ($data != null) {
            return $data->CD_APP;
        } else {
            return null;
        }
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }



    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }



    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }



    /**
     * @param string $table
     */
    public function resetTable()
    {
        $this->table = 'APP_LIST';
    }



    /**
     * @param null $field
     * @param null $alias
     * @return mixed
     */
    public function maximum($field=null,$alias=null)
    {
        return $this->max('CD_APP','NOMBRE')->NOMBRE;
    }
}