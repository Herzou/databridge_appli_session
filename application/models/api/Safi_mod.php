
<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Safi_mod extends MY_Model
{

    private $errors = array();
    protected $table = 'USERAPPS';

    public function __construct()
    {
        parent::__construct();
    }


    public function usersList($cdbur,$app)
    {
        return $this->read('USERNAME,EMAIL,MATRICULE,NOM,PRENOM,MPASS,APP_NOM,ACTIF,CD_BUR,SERVICE,PROFILE,ROLE_1,ROLE_2,SERVICE',array('CD_BUR'=>$cdbur,'APP_NOM'=>$app));
    }


    public function userInfo($im){
        return $this->read('USERNAME,EMAIL,MATRICULE,NOM,PRENOM,MPASS,APP_NOM,ACTIF,CD_BUR,SERVICE,PROFILE,ROLE_1,ROLE_2',array('MATRICULE'=>$im,'APP_NOM'=>'SAFI'));
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }


}