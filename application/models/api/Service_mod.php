<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_mod extends MY_Model
{

    protected $data = array();
    protected $table = 'SERVICES';

    public function __construct()
    {
        parent::__construct();
    }



    public function burInfo($cdbur)
    {
        $response = array();
        //$datas = $this->readLine(null, array('MATRICULE' => $user));
        $datas = $this->readLine(null,array('CD_BUR'=>$cdbur));
        if (is_object($datas)) {
            return $datas;
        } else {
            return $response;
        }
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }



}