<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api_mod extends MY_Model
{
    protected $data = array();
    protected $table = 'USER_DATAS';


    function __construct()
    {
        parent::__construct();
        $this->load->library('Apidata', null, 'apidata');

    }

    public function applicationListe()
    {
        return $this->apidata->listeApp();
    }


    public function userInfos($id)
    {
        $response = array();
        $datas = $this->readLine(null, array('MATRICULE' => $id));
        if (is_object($datas)) {
            return (array)$datas;
        } else {
            return $response;
        }
    }

    public function userDetails($user)
    {
        $response = array();
        $datas = $this->readLine(null, array('USERNAME' => $user));
        if (is_object($datas)) {
            return (array)$datas;
        } else {
            return $response;
        }
    }


    public function userListe($string)
    {
        $response = array();
        //$datas = $this->readLine(null, array('MATRICULE' => $user));
        $datas = $this->read(array('select'=>'*','where'=>" MATRICULE LIKE '$string%'"));
        if (!empty($datas)) {
            return $datas;
        } else {
            return $response;
        }
    }


    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }


}