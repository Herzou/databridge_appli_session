<?php defined('BASEPATH') or exit('No direct script access allowed');

class User_mod extends MY_Model
{
    private $data = array();
    private $message = '';
    private $password = '';

    protected $table = 'USERS_INFOS';


    public function __construct()
    {
        parent::__construct();
        $this->load->library('Users', null, 'user');
        $this->load->library('Grhdata', null, 'grhdb');
        $this->load->library('Utilisateur', null, 'utilisateur');
        $this->load->model('Mailing', 'emailer');
    }

    public function registerUser($datas)
    {
        $password = $this->grhdb->generatePaswd(8);
        $pswd = hash('sha256', $password); //mot de passe haché en 256bits
        $statut = 'encours';
        $usertype = 'user';
        $date = date('d/m/Y H:i:s');
        $type = $datas['type'];
        $services = $datas['service'];
        $centre = $this->getService($services);
        $file = $datas['attach'];

        if ($type == 'Interne') {
            $matricule = $datas['im'];
            $direction = $datas['direction'];
            $service = $centre['SERV'];
            $fonction = $datas['fonction'];
            $grade = $datas['grade'];
            $etab = 'MFB/SG/DGI';
            $cd_bur = $centre['BUR'];
        } elseif ($type == 'Externe') {
            $matricule = null;
            $direction = null;
            $service = $datas['dir'];
            $fonction = $datas['fonc'];
            $grade = null;
            $etab = $datas['etab'];
            $cd_bur = '00-0-000';
        }

        $user_data = array(
            'type' => $type,
            'im' => $matricule,
            'statut' => $statut,
            'userType' => $usertype,
            'dateAdd' => $date,
            'pass' => $pswd,
            'direction' => $direction,
            'service' => $service,
            'cd_bur' => $cd_bur,
            'centre' => $centre['CENTRE'],
            'fonction' => $fonction,
            'grade' => $grade,
            'etab' => $etab,
            'mail' => $datas['email'],
            'nom' => $datas['nom'],
            'prenom' => $datas['prenom'],
            'cin' => str_replace(" ", "", $datas['cin']),
            'tel' => $datas['tel'],
            'login' => $datas['login'],
            'fichier' => $file
        );
        $utilisateur = $this->utilisateur;
        //var_dump($user_data);
        $add = $this->grhdb->registerUser($user_data);

        //$add=false;

        if ($add == true) {
            $im = $user_data['im'];
            $utilisateur->setMATRICULE($im);
            $params['pass'] = $password;
            $params['url'] = PORTAL_LINK . 'register/activation/';
            $this->password = $password;
            $send = $this->emailer->activationEmail($utilisateur, $params);

            if ($send == true) {
                $this->message = 'Votre compte est crée avec succès. Dans quelque minutes, un email contenant le lien d\'activation de votre nouveau compte DGI-Portal sera envoyé à l\'adresse: <em><a href="mailto:' . $user_data['mail'] . '">' . $user_data['mail'] . '</a></em><br>
                 SVP, veuillez consulter cette boite email et activer votre compte.';
                return true;
            } else {
                $del = $this->grhdb->rollbackRegister($utilisateur->getUSERNAME(), $utilisateur->getEMAIL());
                $this->message = 'Erreur : Impossible d\'envoyer l\'email d\'activation du nouveau compte. Veuillez contacter l\'administrateur à admin-portal@gmail.com';
                return false;
            }
        } else {
            $this->message = 'Erreur : Impossible d\'enregistrer l\'utilisateur';
            return false;
        }
    }

    /*************************************************************
     * verifier la validité d'un token d'activation.
     * @param array $datas
     * @return bool true / false
     *************************************************************/
    public function checkAct($datas)
    {
        $user = $datas['user'];
        $token = $datas['tk'];
        $verif = $this->grhdb->checkact($user, $token);
        if ($verif == true) {
            $actif = $this->grhdb->activateUser($user, $token);
            if ($actif == true) {
                $this->message = 'compte activé';
                return true;
            } else {
                $this->message = '<p><h2>ERREUR DE CONNEXION AU SERVEUR :</h2> impossible d\'activer le compte</p>';
                return false;
            }
        } else {
            $this->message = '<p>La page que vous essayez d\'afficher est inéxistant <em>(le lien d\'activation est déja expiré)</em> ou contient une ou  des paramètres invalides.</p>';
            return false;
        }
    }


    /*************************************************************
     * verifier la validité d'un token d'activation.
     * @param array $datas
     * @return bool true / false
     *************************************************************/
    public function checkRestk($datas)
    {
        $user = $datas['user'];
        $token = $datas['tk'];
        $verif = $this->grhdb->checkRestk($user, $token);
        if ($verif == true) {
            $this->message = '';
            return true;
        } else {
            $this->message = '<p>La page que vous essayez d\'afficher est inéxistant <em>(le lien d\'activation est déja expiré)</em> ou contient une ou  des paramètres invalides.</p>';
            return false;
        }
    }


    /**************************************************************
     * VERIFIER L'UTILISATEUR.
     * @param input String $username
     * @return bool
     *************************************************************/
    public function checkUser($username)
    {
        $test = $this->user->check_user($username);
        if ($test == true) {
            $nbr = $this->user->countUser($username);
            $this->message = $nbr;
            return false;
        } else {
            $nbr = $this->user->countUser($username);
            $this->message = $nbr;
            return true;
        }
    }


    /**************************************************************
     * VERIFIER L'ADRESSE EMAIL
     * @param $email
     * @return mixed
     *************************************************************/
    public function unikMail($email)
    {
        $nbr = $this->user->checkmail($email);
        if ($nbr >= 1) {
            $this->message = 'Email déja existant.';
            return false;
        } else {
            $this->message = $nbr;
            return true;
        }
    }


    /**************************************************************
     * VERIFIER LE NUMERO DE CIN
     * @param $email
     * @return mixed
     *************************************************************/
    public function unikCin($cin)
    {
        $nbr = $this->user->checkcin($cin);
        if ($nbr >= 1) {
            $this->message = 'Numéro CIN déja existant.';
            return false;
        } else {
            $this->message = $nbr;
            return true;
        }
    }


    /**
     * VALIDER LE RESET_TOKEN.
     * @param $cin
     * @param $email
     * @return bool
     */
    public function validateReset($cin, $email)
    {
        $valid = $this->grhdb->prepare_reset($cin);

        if ($valid == true) {
            $this->utilisateur->setCIN($cin);
            $data = $this->utilisateur;

            if ($email == $data->getEMAIL()) {
                /*
                 * ENVOIE D'UN EMAIL DE REINITIALISATION.
                 */
                $send = $this->emailer->passwordEmail($data);
                //$send=false;
                if ($send == true) {
                    $this->message = '<div class="alert alert-success">Un email contenant un lien de réinitialisation de votre mot de passe est envoyé à l\'adresse : <em>' . $email . '</em>.<br>Veuillez ouvrir cet email, cliquez sur le lien indiqué pour enregistrer votre nouveau mot de passe.</div>';
                    $this->data = $data;
                    $statut = true;
                } else {
                    $this->message = '<div class="alert alert-danger">Erreur de connexion au serveur  veuillez reéssayer encore. <br> Si ce problème persiste, veuillez contacter l\'administrateur en cliquant ce <em>' . anchor(PORTAL_HOME . 'contact', ' LIEN ', array('target' => '_blank', 'class' => 'link')) . '</em>';
                    $this->data = array();
                    $statut = false;
                }
            } else {
                $this->message = '<div class="alert alert-danger">Cette adresse email est incorrecte. Votre nom d\'utilisateur est : <em><strong>"' . $data->getUSERNAME() . '"</strong></em>.</div>';
                $statut = false;
                $this->data = array();
            }
        } else {
            $this->message = '<div class="alert alert-danger">Ce numéro de CIN n\'est pas enregistré ou inéxistant dans notre système.</div>';
            $statut = false;
            $this->data = array();
        }

        return $statut;
    }


    /**
     * METTRE A JOUR LE MOT DE PASSE.
     * @param array $datas
     */
    public function updatePass($datas)
    {
        $update = $this->grhdb->updatePass($datas);
        return $update;
    }


    public function applistType($user)
    {
        $apps = array();
        $listes = $this->grhdb->userApps($user);
        foreach ($listes as $key => $list) {
            $apps[$list->SLUG] = $list->NOM;
        }
        return $apps;
    }


    public function userApplis($user)
    {
        $res = array();
        $this->setTable('USERAPPS');
        $apps = $this->read(null, array('USERNAME' => $user));
        foreach ($apps as $key => $list) {
            $res[$list->APP_NOM] = $list->SLUG;
        }
        $this->resetTable();
        return $res;
    }


    public function userInfos($user)
    {
        return $this->readLine(null, array('USERNAME' => $user));
    }


    public function demandeApp($datas)
    {
        $username = $datas['user'];
        $user = $this->utilisateur;
        $this->utilisateur->setUSERNAME($username);
        $listes = array();
        if (isset($datas['liste'])) {
            $listes = $datas['liste'];
        }

        $detail = $datas['details'];

        //Mettre à jour l'Info utilisateur.
        $update = $this->grhdb->updateUserStatut($username);

        if ($update == true) {
            //Envoyer l'email de notification.
            $send = $this->emailer->demadeEmail($user, $listes, $detail);
            return $send;
            //return true;
        } else {
            return false;
        }
    }


    protected function getService($services)
    {
        $serv = explode("+", $services);
        return array(
            'BUR' => $serv[0],
            'SERV' => $serv[1],
            'CENTRE' => $serv[2],
        );
    }


    public function contactAdmin($datas)
    {
        $send = $this->emailer->contactAdmin($datas);
        if ($send == TRUE) {
            $this->message = '<div class="alert alert-success">Le message à été envoyé avec succès.</div>';
        } else {
            $this->message = '<div class="alert alert-danger">ERREUR : Echec de l\'envoi de votre message.</div>';
        }
        return $send;
    }

    /*GEETERS AND SETTERS */

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @param string $table
     */
    public function resetTable()
    {
        $this->table = 'USERS_INFOS';
    }
}
