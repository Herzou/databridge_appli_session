<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Key_mod extends CI_Model
{
  protected $data = array();
  
  
  function __construct()
  {
    parent::__construct();
  }
 
  public function key($key)
  {
     $hkey = hash('sha256',$key);
     $dkey = trim(file_get_contents(KEY),' ');
     if ($hkey==$dkey ){
     	return true;
     }else{
        return false;
     }
  }
}