<?php

class Apps_mod extends MY_Model
{

	protected $table = 'APP_LIST';

	public function __construct()
	{
		parent::__construct();
		$this->datasource('LOCAL');
		$this->load->library('Users', null,'user');
	}


	public function defaultApps()
	{
		$apps = array();
         $defaults =  $this->read(null,array('DEFAULT_APP'=>'yes'));
         if (!empty($defaults)){
			 foreach ($defaults as $key => $app) {
				 $apps[$app->APP_NOM] = $app;
			 }
		 }

         return $apps;
	}

	public function userApps($user)
	{
		$userapps = $this->user->applist($user);
		$defaultapps = $this->defaultApps();

        return array_merge($defaultapps,$userapps);
	}


	/**
	 * @return string
	 */
	public function getTable()
	{
		return $this->table;
	}

	/**
	 * @param string $table
	 */
	public function setTable($table)
	{
		$this->table = $table;
	}
	/**
	 * @param string $table
	 */
	public function resetTable()
	{
		$this->table = 'APP_LIST';
	}

}
