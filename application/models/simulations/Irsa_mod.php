<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Irsa_mod
{
    protected $IRSA = 2000; //Minimun à payer....
    protected $baseSalaire = 0;

    //INITIALISATION - A parametrer et a intégrer dans les BD
    private $tranche = array(
        array(600000, 0.20),
        array(500000 , 0.15),
        array(400000 , 0.10),
        array(350000 ,  0.05)
    );


    private function calculIrsa($salaire){
        $irsa = $this->resTranche($salaire,$this->tranche);
        if ($irsa>=2000){
            $this->setIRSA($irsa);
        }
    }

    private function resTranche($salaire, $tranche){
        //il faut au moins une tranche
        if(count($tranche) === 0) return 0;

        //Calculer la première tranche
        $res = max($salaire - $tranche[0][0], 0) * $tranche[0][1];

        //Calculer les tranches suivantes
        for($i=1; $i<count($tranche); $i++){
            $res +=  min(max($salaire - $tranche[$i][0],0), $tranche[$i-1][0] - $tranche[$i][0])*$tranche[$i][1];
        }
        //IRSA>2000
        return max($res,2000);
    }

    private function roundX($x){
        return floor($x/100)*100 ;
    }

    /**
     * @return int
     */
    public function getBaseSalaire()
    {
        return $this->baseSalaire;
    }

    /**
     * @param int $baseSalaire
     */
    public function setBaseSalaire($baseSalaire)
    {
        $this->baseSalaire = $this->roundX($baseSalaire);
        $this->calculIrsa($this->baseSalaire);
    }



    /**
     * @return int
     */
    public function getIRSA()
    {
        return $this->IRSA;
    }

    /**
     * @param int $IRSA
     */
    public function setIRSA($IRSA)
    {
        $this->IRSA = $IRSA;
    }


}