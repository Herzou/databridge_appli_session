<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Impots_mod extends MY_Model
{
    protected $table = '';

    private $sb = 0;
    private $accessoires = 0;
    private $cnaps = 0;
    private $avantages = 0;
    private $cotisation = 0;
    private $deductions = 0;


    private $tauxIrsa = 0;
    private $montant = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('simulations/irsa_mod','bareme');
    }



    public function calculIrsa($params)
    {

        if ($params['sb']){
            foreach ($params as $key=>$param){
                if ($param!=null){
                    $this->$key = $param;
                }
            }
            //Formule de base IRSA 2020
            $BaseImposableIRSA = $this->sb + $this->accessoires - $this->cnaps - $this->cotisation + $this->avantages - $this->deductions;

            $this->bareme->setBaseSalaire($BaseImposableIRSA);
            $this->setMontant($this->bareme->getBaseSalaire());

            return $this->bareme->getIRSA();
        }else {
            return 0;
        }
    }

    /**
     * @return int
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param int $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }



    /**
     * @return int
     */
    public function getTauxIrsa()
    {
        return $this->tauxIrsa;
    }

    /**
     * @param int $tauxIrsa
     */
    public function setTauxIrsa($tauxIrsa)
    {
        $this->tauxIrsa = $tauxIrsa;
    }




    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @param string $table
     */
    public function resetTable()
    {
        $this->table = '';
    }
}