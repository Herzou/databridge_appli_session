<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 15/05/2018
 * Time: 22:35
 */
class Security_mod extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function unlockSecure($string)
    {
        $result = '';
        $offset = 2;
        $length = 5;

            $chars = str_split(trim($string,' '));
            foreach ($chars as $value) {
                $result = $result.(chr(ord($value) + $offset));
                if ($offset<$length) {
                    $offset ++;
                }else{
                    $offset = 2;
                }
            }

            return $result;
        }

}