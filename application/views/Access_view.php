<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>php chiffre en lettre</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="jumbotron">
        <h1>TEST TEMPLATE CI</h1>
    </div>
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown button
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton01">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </div>
    <div class="row">
        <a id="link001" href="" class="btn btn-link">link</a>
    </div>
    <div class="row" id="main">
        <h2>test</h2>
    </div>
</div>
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
</body>
</html>