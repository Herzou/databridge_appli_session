<?php 
    var_dump($_COOKIE);
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Api Webservice</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/loader.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/lp_frame.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/mystyle.css.css'); ?>">

</head>
<body style="background-color: #536A7F;">
<div class="container" style="background-color: #ffffff;">
    <div class="row" style="margin: 25px;">
        <h1 class="title">Data-Bridge</h1>
        <span class="lead">Webservice entre les applications de la DGI</span>
    </div>
    <div class="row">

    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.1.1.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

</body>
</html>