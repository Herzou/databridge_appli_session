<?php

/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 12/07/2017
 * Time: 14:31
 */
//define('OCIBD', dirname(__FILE__));

include_once("Conf.php");

class Ocidb
{
    public static $conn;
    public static $execMode = OCI_COMMIT_ON_SUCCESS;
    private static $sql = '';
    private static $execute = TRUE;
    private static $tablename = null;
    
    public static function getSql() {
        return self::$sql;
    }

    public static function getExecute() {
        return self::$execute;
    }

    public static function setSql($sql) {
        self::$sql = $sql;
    }

    public static function setExecute($execute) {
        self::$execute = $execute;
    }

        /**
     * @return int
     */
    public static function getExecMode()
    {
        return self::$execMode;
    }

    /**
     * @param int $execMode
     */
    public static function setExecMode($execMode)
    {
        self::$execMode = $execMode;
    }



    //connection à la base de donnée
    public static function init()
    {
         $utilisateur = Conf::getLogin();
         $motdepasse = Conf::getPassword();
         $host = Conf::getHostname();
         $sid = Conf::getDatabase();
         $db="(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=$host)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=$sid)))";
            // Connexion au service XE (i.e. la base de données) sur la machine "localhost"
            self::$conn = oci_connect($utilisateur, $motdepasse, $db);


        if (!self::$conn) {
            $e = oci_error();   // Pour les erreurs oci_connect, ne passez pas de gestionnaire de connexion
            echo $e['message'].'ERREUR DE CONNEXION A LA BASE DE DONNEE';
            die();
        }

    }

    public static function openTransaction()
    {
        self::$execMode = OCI_NO_AUTO_COMMIT;// passer en mode transaction
    }


    public static function commit()
    {
        oci_commit(self::$conn); // commit

    }


    public static function rollback()
    {
        oci_rollback(self::$conn); // rollback
       /* if ($rollback==true){
            self::$execMode = OCI_COMMIT_ON_SUCCESS; // sortie du mode "transaction"
        }*/
        self::$execMode = OCI_COMMIT_ON_SUCCESS; // sortie du mode "transaction"
    }

    public static function endTransaction(){
        self::$execMode = OCI_COMMIT_ON_SUCCESS; // sortie du mode "transaction" et retour en mode normal.
    }

    /***************************
     * RECUPERER TOUT DANS UNE TABLE
     * @param $tablename
     * @return array()
     ****************************/
    public static function selectAll($tablename,$param=null){
        $sql = "SELECT * FROM  $tablename";
        $output = array();
        if (!empty($param) && $param!=null) {
            $sql .= " WHERE";
            $i=0;
            foreach($param as $key => $value){
                if ($i==0){
                    if ($value==null ){
                        $sql.=" $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else {
                        $sql .= " $key ='$value'";
                    }
                }else{
                    if ($value==null){
                        $sql.=" AND $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" AND $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" AND $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" AND $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" AND $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else{
                        $sql .= " AND $key ='$value'";
                    }
                    //$sql.=" AND $key ='$value'";
                }
                $i++;
            }
        }

        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        while ( ($data=oci_fetch_object($stid))!=false ){
        		$output[] = $data;
        }

        //empty array for null
        return $output;
    }

    /***************************
     * RECUPERER TOUT DANS UNE TABLE
     * @param $tablename
     * @return array()
     ****************************/
    public static function findAll($tablename,$param=null){
        $sql = "SELECT * FROM  $tablename";
        $output = array();
        if (!empty($param) && $param!=null) {
            $sql .= " WHERE";
            $i=0;
            foreach($param as $key => $value){
                if ($i==0){
                    if ($value==null ){
                        $sql.=" $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" $key =TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else {
                        $sql .= " $key ='$value'";
                    }
                }else{
                    if ($value==null){
                        $sql.=" AND $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" AND $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" AND $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" AND $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" AND $key =TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else{
                        $sql .= " AND $key ='$value'";
                    }
                    //$sql.=" AND $key ='$value'";
                }
                $i++;
            }
        }
        //var_dump($sql);
        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        while ( ($data=oci_fetch_object($stid))!=false ){
        		$output[] = $data;
        }

        //empty array for null
        return $output;
    }

    /***************************
     * RECUPERER DES COLONES DANS UNE TABLE
     * @param $tablename
     * @return array()
     ****************************/
    public static function findCols($tablename,$colons,$param=null){
        $field = '';
        foreach($colons as $index){
            $colname = explode("@",$index);
            if (isset($colname[1]) && $colname[1]!=null && $colname[1]!=''){
            	$value = $colname[0];
                $alias = $colname[1];
                if (substr($value,0,5)=='date*'){
                    $field.= ',TO_CHAR('.substr($value,5).',\'DD/MM/YYYY\') AS '.$alias;
                }else{
                    $field.= ','.$value.' AS '.$alias;
                }
            }else{
                $value = $colname[0];
                if (substr($value,0,5)=='date*'){
                    $field.= ',TO_CHAR('.substr($value,5).',\'DD/MM/YYYY\') AS '.substr($value,5);
                }else{
                    $field.= ','.$value;
                }
            }
        }
        $fields = trim($field,",");
        $sql = "SELECT DISTINCT $fields FROM  $tablename";
        $output = array();
        if (!empty($param) && $param!=null) {
            $sql .= " WHERE";
            $i=0;
            foreach($param as $key => $value){
                if ($i==0){
                    if ($value==null ){
                        $sql.=" $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else {
                        $sql .= " $key ='$value'";
                    }
                }else{
                    if ($value==null){
                        $sql.=" AND $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" AND $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" AND $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" AND $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" AND $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else{
                        $sql .= " AND $key ='$value'";
                    }
                    //$sql.=" AND $key ='$value'";
                }
                $i++;
            }
        }
        //var_dump($sql);
        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        while ( ($data=oci_fetch_object($stid))!=false ){
        		$output[] = $data;
        }

        //empty array for null
        return $output;
    }


    /************************
     * SELECTIONNER UNE LIGNE OU PLUSIEURRS DANS UNE TABLE
     * @param $tablename
     * @param null $param
     * @return object or false
     */
    public static function selectRow($tablename,$param=null){
        $sql = "SELECT * FROM $tablename";
        if (!empty($param) && $param!=null) {
            $sql .= " WHERE";
            $i=0;
            foreach($param as $key => $value){
                if ($i==0){
                    if ($value==null ){
                        $sql.=" $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else {
                        $sql .= " $key ='$value'";
                    }
                }else{
                    if ($value==null){
                        $sql.=" AND $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" AND $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" AND $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" AND $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" AND $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else{
                        $sql .= " AND $key ='$value'";
                    }
                    //$sql.=" AND $key ='$value'";
                }
                $i++;
            }
        }
        //var_dump($sql);
        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        $data=oci_fetch_object($stid);
        $output = $data;

        //false value for null
        return $output;
    }


    /*****************
     *SELECTONNER UN OU PLUSIEURS COLONES D'ENREGISTREMENT DANS UNE TABLE
     *@table : <String> Nom de la table selectonnée
     *@Cols : <Array> Liste des colones à selectionner
     *@condition :<Array> Condition de selection des données (WHERE)
     *@condition = array(array('col1','operateur','valeur1','ASSOCIATIVITE','DataType'),
    array('col2','operateur','valeur2','ASSOCIATIVITE','DataType'),
    .......... ) ******
     *@ASSOCIATIVITE :<String> Operateur AND ou OR  ou toujrs NULL pour le 1er condition.
     *@DataType :<String> 'num' pour numerique ou booléen, 'str' pour les chaines et dates
     *@retour :<array> tableau d'objets.
     ******************/
    public static function selectCols($tablename, $colonnes, $param=null)
    {
        $output = array();
        $field = '';
        foreach($colonnes as $value){
            if (substr($value,0,4)=='date'){
                $field.= ',TO_CHAR('.substr($value,5).',\'DD/MM/YYYY\') AS '.substr($value,5);
            }else{
                $field.= ','.$value;
            }
        }
        $fields = trim($field,",");
        $sql = "SELECT DISTINCT $fields FROM $tablename";
        if (!empty($param) && $param!=null) {
            $sql .= " WHERE";
            $i=0;
            foreach($param as $key => $value){
                if ($i==0){
                    if ($value==null ){
                        $sql.=" $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else {
                        $sql .= " $key ='$value'";
                    }
                }else{
                    if ($value==null){
                        $sql.=" AND $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" AND $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" AND $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" AND $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" AND $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else{
                        $sql .= " AND $key ='$value'";
                    }
                    //$sql.=" AND $key ='$value'";
                }
                $i++;
            }
        }
        $sql.= " ORDER BY ".$colonnes[0];
        //var_dump($sql);
        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        while ( ($data=oci_fetch_object($stid))!=false ){
            $output[] = $data;
        }

        //empty array for null
        return $output;
    }

    /********************************************************************
     * COMPTER LE NOMBRE D'ENREGISTREMENT DANS D'UNE TABLE OU REQUETE
     * @param $tablename
     * @param $colon
     * @param null $param
     * @return int
     *******************************************************************/
    public static function countRows($tablename,$colon,$param=null){
        $sql = "SELECT DISTINCT COUNT($colon) AS NOMBRE  FROM $tablename";
        $output = 0;
        if (!empty($param) && $param!=null) {
            $sql .= " WHERE";
            $i=0;
            foreach($param as $key => $value){
                if ($i==0){
                    if ($value==null ){
                        $sql.=" $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else {
                        $sql .= " $key ='$value'";
                    }
                }else{
                    if ($value==null){
                        $sql.=" AND $key IS NULL";
                    }elseif($value=='notnull'){
                        $sql.=" AND $key IS NOT NULL";
                    }elseif(substr($value,0,2)=='#='){
                        $res = substr($value,2);
                        $sql.=" AND $key<>'$res'";
                    }elseif (substr($value,0,4)=='num*'){
                        $sql.=" AND $key =".substr($value,4);
                    }elseif(substr($value,0,5)=='date*'){
                        $sql.=" AND $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                    }else{
                        $sql .= " AND $key ='$value'";
                    }
                    //$sql.=" AND $key ='$value'";
                }
                $i++;
            }
        }
        //var_dump($sql);
        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        $data=oci_fetch_object($stid);
            $output = $data->NOMBRE;

        //0 for null
        return $output;
    }


    /********************
     * INSERER UNE LIGNE DANS UNE TABLE.
     * @param $tablename
     * @param $params
     * @return bool
     **********************/
    public static function addData($tablename,$params){
        $fields='';
        $valeur='';
        foreach($params as $key => $value){
            $fields.= ','.$key;
            if (substr($value,0,5)=='date*'){
                $valeur .= ",TO_DATE('".substr($value,5)."','DD/MM/YYYY HH24:MI:SS')";
            }elseif (substr($value,0,4)=='num*'){
                $valeur.= ",".substr($value,4);
            }elseif($value==null){
                $valeur.= ",".$value;
            }else{
                $valeur.= ",'".str_replace(array("'","(",")"),array("''","-","-"),$value)."'";
            }
        }

        $input = trim($fields,",");
        $data = trim($valeur,",");



        $sql = "INSERT INTO $tablename ($input) VALUES ($data)";
        self::$sql[]=$sql;
         
        if (self::$execute == TRUE) {
            $stid = oci_parse(self::$conn,$sql);
            $statut = oci_execute($stid,self::$execMode);            
        }else{
            $statut = FALSE;
        }
        return $statut;

    }

    /***************************************************
     * METTRE A JOUR UNE OU DES LIGNES D'ENREGISTREMENTS D'UNE TABLE
     * @param $tablename
     * @param $datas
     * @param $params
     * @return bool
     ****************************************************/
    /**
     * 
     * @param type $tablename
     * @param type $datas
     * @param type $params
     * @return type bool
     */
    public static function updateData($tablename,$datas,$params)
    {
        $sql = "UPDATE $tablename SET ";
        $i=0;
        foreach($datas as $key => $value){
            if ($i==0){
                if (substr($value,0,5)=='date*'){
                    $sql.="$key=TO_DATE('".substr($value,5)."','DD/MM/YYYY HH24:MI:SS')";
                }elseif (substr($value,0,4)=='num*'){
                    $sql.="$key=".substr($value,4);
                }else{
                    $sql.="$key='".$value."'";
                }

            }else{
                if (substr($value,0,5)=='date*'){
                    $sql.=", $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY HH24:MI:SS')";
                }elseif (substr($value,0,4)=='num*'){
                    $sql.=", $key=".substr($value,4);
                }else{
                    $sql.=", $key='".$value."'";
                }
            }
            $i++;
        }
        $sql.=" WHERE";
        $j=0;
        foreach($params as $cle => $vals){
            if ($j==0){
                if (substr($vals,0,4)=='num*'){
                    $sql.=" $cle =".substr($vals,4);
                }elseif(substr($vals,0,5)=='date*'){
                    $sql.=" $cle =TO_DATE('".substr($vals,5)."','DD/MM/YYYY')";
                }else{
                    $sql.=" $cle ='$vals'";
                }
            }else{
                if (substr($vals,0,4)=='num*'){
                    $sql.=" AND $cle =".substr($vals,4);
                }elseif(substr($vals,0,5)=='date*'){
                    $sql.=" AND $cle =TO_DATE('".substr($vals,5)."','DD/MM/YYYY')";
                }else{
                    $sql.=" AND $cle ='$vals'";
                }
            }
            $j++;
        }
        //var_dump($sql);
        //die();
        self::$sql[]=$sql;
         
        if (self::$execute == TRUE) {
            $stid = oci_parse(self::$conn,$sql);
            $statut = oci_execute($stid,self::$execMode);            
        }else{
            $statut = FALSE;
        }
        return $statut;
        /*
        $stid = oci_parse(self::$conn,$sql);
        $statut = oci_execute($stid,self::$execMode);

        return $statut;
         */
    }

    /*******************************
     * SUPPRIMER UNE OU PLUSIEURS lIGNE D'ENREGISTREMENT D'UNE TABLE
     * @param $tablename
     * @param $param
     * @return bool
     *********************************/
    public static function deleteData($tablename, $param)
    {
        $sql = "DELETE FROM $tablename WHERE ";
        $i=0;
        foreach($param as $key => $value){
            if ($i==0){
                if ($value==null ){
                    $sql.=" $key IS NULL";
                }elseif($value=='notnull'){
                    $sql.=" $key IS NOT NULL";
                }elseif(substr($value,0,2)=='#='){
                    $res = substr($value,2);
                    $sql.=" $key<>'$res'";
                }elseif (substr($value,0,4)=='num*'){
                    $sql.=" $key =".substr($value,4);
                }elseif(substr($value,0,5)=='date*'){
                    $sql.=" $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                }else {
                    $sql .= " $key ='$value'";
                }
            }else{
                if ($value==null){
                    $sql.=" AND $key IS NULL";
                }elseif($value=='notnull'){
                    $sql.=" AND $key IS NOT NULL";
                }elseif(substr($value,0,2)=='#='){
                    $res = substr($value,2);
                    $sql.=" AND $key<>'$res'";
                }elseif (substr($value,0,4)=='num*'){
                    $sql.=" AND $key =".substr($value,4);
                }elseif(substr($value,0,5)=='date*'){
                    $sql.=" AND $key=TO_DATE('".substr($value,5)."','DD/MM/YYYY')";
                }else{
                    $sql .= " AND $key ='$value'";
                }
                //$sql.=" AND $key ='$value'";
            }
            $i++;
        }
        //var_dump($sql);
        $stid = oci_parse(self::$conn,$sql);
        $statut = oci_execute($stid,self::$execMode);
        if(!$statut){
            return false;
        }else{
            return true;
        }
    }


    /********************************************
     * EXECUTER UNE REQUETE SQL
     * @param $sql
     * @return array
     *******************************************/
    public static function executeAll($sql)
    {
        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        while ( ($data=oci_fetch_object($stid))!=false ){
            $output[] = $data;
        }

        //empty array for null
        return $output;
    }

	
    /********************************************
     * EXECUTER UNE REQUETE SQL
     * @param $sql
     * @return array
     *******************************************/
    public static function executeRow($sql)
    {
        //var_dump($sql);
        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        $data=oci_fetch_object($stid);
        $output = $data;

        //false value for null
        return $output;
    }


    public static function storetable($table){
        self::$tablename = $table;
        $query = "SELECT COLUMN_NAME,DATA_TYPE FROM user_tab_columns WHERE table_name = '".$table."'";

        $stid = oci_parse(self::$conn,$query);
        oci_execute($stid,self::$execMode);

        while ( $table_infos = oci_fetch_assoc($stid) ){
            $output[$table_infos['COLUMN_NAME']] = $table_infos['DATA_TYPE'];
        }

        return $output;
    }


    public static function select($table){
        $select = 'SELECT ';
        $from = ' FROM '.$table;
        $list = self::storetable($table);
        foreach($list as $key => $value){
            switch ($value) {
                case 'DATE':
                    $select .= 'TO_CHAR('.$key.',\'DD/MM/YYYY\') AS '.$key.',';
                    break;
                case 'TIMESTAMP':
                    $select .= 'TO_CHAR('.$key.',\'DD/MM/YYYY\') AS '.$key.',';
                    break;

                default :
                    $select .= $key.',';
            }
        }

        $sql = trim($select,",").' '.$from.' '.$table;

        $stid = oci_parse(self::$conn,$sql);
        oci_execute($stid,self::$execMode);
        while ( $tables = oci_fetch_object($stid) ){
            $output[] = $tables;
        }
        return $output;
    }

    public static function do_query($sql)
    {
        $stid = oci_parse(self::$conn,$sql);
        $execute = oci_execute($stid,self::$execMode);
        return $execute;
    }

}