<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 13/09/2017
 * Time: 09:36
 */
require APPPATH . 'libraries/sq/Ocidb.php';

class Sqdata extends Ocidb {

    private $errors = array();
    private $response = array();
    private $today;
    private $query;

    public function __construct() {
        parent::init();
        $this->errors = array();
        $this->today = date("d/m/Y");
    }

    public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function setErrors($errors) {
        $this->errors = $errors;
    }

    public function getResponse() {
        return $this->response;
    }

    public function setResponse($response) {
        $this->response = $response;
    }

    public function dossiersList(){
        $list = $this->findAll('DOSSIER_CONTROLE');
        return $list;
    }

    public function dossier($ncu=null){
        $list = $this->selectRow('DOSSIER_CONTROLE',array('NCU'=>$ncu));
        return $list;
    }

    public function adddossControl($array)
    {
        if (is_array($array)){
            $params['NIF'] = $array['nif'];
            $params['STATUT'] = 0;
            $params['BUR'] = $array['cdbur'];
        	$add = $this->addData('DOSSIER_CREATION',$params);
            return $add;
        }else{
            return false;
        }
    }
}
