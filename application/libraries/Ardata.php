<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 13/09/2017
 * Time: 09:36
 */
require APPPATH . 'libraries/ar/Ocidb.php';

class Ardata extends Ocidb {


    public function __construct(){
        parent::init();
    }

    public function analiseRisk($nif,$annee){
        $list = $this->findAll('AR_RESULTAT_COMMUNIQUE',array('RES_NIF'=>$nif,'RES_ANNEE'=>'num*'.$annee));
        return $list;
    }

    public function analiseRisque($nif){
        $list = $this->findAll('AR_RESULTAT_COMMUNIQUE',array('RES_NIF'=>$nif));
        return $list;
    }

    public function arIndicateur($nif,$annee,$score=null){
        $list = $this->findAll('AR_RESULTAT_INDICATEUR',array('RES_NIF'=>$nif,'RES_ANNEE'=>'num*'.$annee));
        return $list;
    }
}
