<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'grh/Ocidb.php';
include_once 'grh/Codes.php';

class Users extends Ocidb {

    private $USER_NAME = null; //nom d'utilisateur
    private $MATRICULE = null; //num matricule
    private $DIR_NAME = null; //bureau fiscal
    private $OPER_TYPE = null;
    private $OPER_TK = null;
    private $DIR_CODE = null; //code bureau de l'utilisateur
    private $KEY = null;
    private $VALID_CDBUR = null; //code bureau validateur
    private $VALID_BUREAU = null;  //Libellé bureau validateur
    private $USER_CODE = null;
    private $EMAIL = null;
    //private $USER_ERROR = array('user' => null, 'pass' => null,'login'=>null);
    private $USER_ERROR = null;
    private $LABEL = null; //Type bureau validateur
    public $statut = false;
    protected $today; //date du jour
    protected $date;

    public function __construct($USER = null) {
        parent::init();


        date_default_timezone_set('Africa/Nairobi');

        $this->today = date('d/m/Y H:i:s');
        $this->date = date('Y-m-d H:i:s');
        if ($USER != null && $USER != '') {
            $this->setUSERNAME($USER);
        }
    }

    //verifier si l'utilisateur existe (type booléen)
    public function check_user($pseudo) {
            $sql = "SELECT COUNT(USERNAME) AS NOMBRE FROM GRH_USERS WHERE STATUT='actif' AND  (USERNAME =:pseudo OR EMAIL =:pseudo)";
            $params = array(':pseudo'=>$pseudo);
            $nbr = $this->do_query($sql,$params);
            if ($nbr->NOMBRE >= 1) {
                return true;
            } else {
                $this->USER_ERROR = 'Nom d\'utilisateur invalide sur le système';
                return false;
            }
    }

    //verifier si le login et le mot de passe sont corrects (retourne un tableau de resultat);
    private function check_pass($user, $pass) {
        $passwd = hash('sha256', $pass);
        $sql = "SELECT CD_ACCESS AS PASSWD FROM GRH_USERS WHERE CD_ACCESS =:passwd AND (USERNAME =:pseudo OR EMAIL =:pseudo)";
        $params = array(
            ':passwd'=>$passwd,
            ':pseudo'=>$user
        );
        $nbr = $this->do_query($sql,$params);
        if ($nbr && $passwd == $nbr->PASSWD) {
            return true;
        } else {
            $this->USER_ERROR = 'Mot de passe incorrect';
            return false;
        }
    }

    //Créer une session valide pour l'utilisateur connecté
    public function connect_user($user, $passwd) {
        $datas = array();
        $check_user = $this->check_user($user);

        if ($check_user == true) {

            $check_pass = $this->check_pass($user, $passwd);

            if ($check_pass == true) {
                $this->statut = true;
                $sess_token = Codes::generate_token();
                $update = $this->addsess_token($user,$sess_token);

                if ($update==true){
                    $infos = $this->getUserinfos($user);
                    $datas = (array)$infos;
                }else{
                    $this->USER_ERROR = 'Erreur de connexion à la base de donnée. Reéssayez!';
                    $this->statut = false;
                }

            }
        }
        return array('statut'=>$this->statut,'data'=>$datas);
    }

    //Les Informations sur un utilisateur connecté.$_COOKIE
    public function userInfos($user,$token){
        $sql = "SELECT * FROM USERS_INFOS WHERE SESS_TOKEN=:token AND (USERNAME =:username OR EMAIL=:username)";
        $params = array(':token'=>$token,':username'=>$user);
        $infos = $this->do_query($sql,$params);

        //return false  for empty.
        return $infos;
    }


    private function updateToken($datas){
        $sql = "UPDATE GRH_USERS SET SESS_TOKEN =:token WHERE USERNAME =:username OR EMAIL =:username";
        $params = array(
            ':username' => $datas['user'],
            ':token' => $datas['token']
        );
        $statut = $this->do_updateQuery($sql, $params);
        return $statut;

    }


    //Creer un nouveau token de session.
    public function addsess_token($user,$sessToken)
    {
        $add = array();
        $ip = $_SERVER['REMOTE_ADDR'];
        //$user_agent = $_SERVER['HTTP_USER_AGENT'];
        $date_add = $this->today;
        $date_expire = $expire = date('d/m/Y H:i:s', strtotime($this->date)+(4*3600)); //expiration pour 4h
        $this->openTransaction();
        $add[] = $this->updateToken(array('user'=>$user,'token'=>$sessToken));
        $params = array('USERNAME'=>$user,
                        'ID_SESSION'=>$sessToken,
                        'DEBUT'=>'date*'.$date_add,
                        'FIN'=>'date*'.$date_expire,
                        //'USER_AGENT'=>$user_agent,
                        'IP_ADDR'=>$ip);
        $add[] = $this->addData('USER_SESSION',$params);
        if (!in_array(false,$add)) {
            $this->commit();
            $this->endTransaction();
            return true;
        }else{
            $this->rollback();
            $this->endTransaction();
            return false;
        }
    }

    //Valider une session utilisateur
    public function validateSession($sessionId){
        $check = $this->countRows('USER_SESSION','ID_SESSION',array('ID_SESSION'=>$sessionId,'EXPIRE'=>'num*0'));
        if ($check==1) {
            $expire = $this->sessionExpire($sessionId);
            if ($expire==true) {
                $this->setUSERERROR('La session est expiré');
                return false;
            }else{
                $this->setUSERERROR('La session est encore valide');
                return true;
            }
        }else{
            $this->setUSERERROR('La session est invalide');
            return false;
        }
    }

    //verifier l'expiration d'une session
    private function sessionExpire($sessionId){
        $sql="SELECT USERNAME, TO_CHAR(DEBUT,'YYYY-MM-DD HH24:MI:SS') AS DEBUT, TO_CHAR(FIN,'YYYY-MM-DD HH24:MI:SS') AS FIN FROM USER_SESSION WHERE ID_SESSION = '$sessionId' AND EXPIRE=0";
        $session = $this->executeRow($sql);
        if ($session!=false) {
            $this->setUSERNAME($session->USERNAME);
            $start  = date('Y-m-d H:i:s',strtotime($session->DEBUT));
            $end    = date('Y-m-d H:i:s',strtotime($session->FIN)); 
            $today = date('Y-m-d H:i:s');
            $d_start    = new DateTime($start);
            $d_end      = new DateTime($today);
            $diff = $d_start->diff($d_end); 
            $expire =(int) $diff->format('%h'); 
            if($expire>=4){
                $this->endsession($sessionId);
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    //Verifier une session  pour un user
    public function check_session() {
        if (isset($_SESSION['svf']['user']) && isset($_SESSION['svf']['oper_tk']) && isset($_SESSION['svf']['dir'])) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }
    
    public function validateToken($user,$tk) {
        $sql = "SELECT COUNT(USERNAME) AS NOMBRE FROM GRH_USERS WHERE SESS_TOKEN =:token AND (USERNAME LIKE :pseudo OR EMAIL LIKE :pseudo)";
        $params = array(':pseudo'=>$user,':token'=>$tk);
        $nbr = $this->do_query($sql,$params);
        if ($nbr->NOMBRE==1) {
            return true;
        }else{
            return false;
        }
    }

    //Validation de la session crée.
    public function validate_user($user,$tk) {
        $session = $this->validateToken($user,$tk);
        if ($session == true) {
            return true;
        } else {
            return false;
        }
    }

    public function applist($user)
    {
        $apps = array();
        $sql = "SELECT * FROM USERAPPS WHERE USERNAME=:username OR EMAIL=:username";
        $params = array(':username'=>$user);
         $list = $this->do_select($sql,$params);

         if (!empty($list)){
         	foreach($list as $key => $value){
         	  //$apps[$value->APP_NOM] = $this->formatAppInfo($value);
         	  $apps[$value->APP_NOM] = $value;
         	}
         }
        return $apps;
    }

    public function formatAppInfo($params)
    {
        $applis = array();

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $applis[$key] = utf8_decode($value);
            }
            return (object)$applis;
        } else {
            return (object)$applis;
        }

    }

    //Compter l'utilisateur indiqué.
    public function countUser($username)
    {
        $sql = "SELECT COUNT(USERNAME) AS NOMBRE FROM GRH_USERS WHERE USERNAME LIKE :pseudo OR EMAIL LIKE :pseudo";
        $params = array(':pseudo'=>$username.'%');
        $nbr = $this->do_query($sql,$params);
        return $nbr->NOMBRE;
    }

    public function checkmail($email)
    {
        $sql = "SELECT COUNT(EMAIL) AS NOMBRE FROM GRH_USERS WHERE EMAIL LIKE :pseudo";
        $params = array(':pseudo'=>$email);
        $nbr = $this->do_query($sql,$params);
        return $nbr->NOMBRE;
    }


    public function checkcin($cin)
    {
        $sql = "SELECT COUNT(CIN) AS NOMBRE FROM PERSONNEL WHERE CIN LIKE :pseudo";
        $params = array(':pseudo'=>$cin);
        $nbr = $this->do_query($sql,$params);
        return $nbr->NOMBRE;
    }


    private function getUserinfos($username)
    {
        $sql = "SELECT * FROM USERS_INFOS WHERE USERNAME =:username OR EMAIL =:username";
        $params = array(':username'=>$username);
        $infos = $this->do_query($sql, $params);
        return $infos;
    }


    public function endsession($sessionId){
        $update = $this->updateData('USER_SESSION',array('EXPIRE'=>'num*1'),array('ID_SESSION'=>$sessionId));
        return $update;
    }


    public function setMATRICULE($im){
        $this->MATRICULE = $im;
    }

    public function getMATRICULE(){
        return $this->MATRICULE;
    }

    /**
     * @return null
     */
    public function getUSERNAME() {
        return $this->USER_NAME;
    }

    /**
     * @param null $USER_NAME
     */
    public function setUSERNAME($USER_NAME) {
        $this->USER_NAME = $USER_NAME;
    }
    
    function getVALIDBUREAU() {
        return $this->VALID_BUREAU;
    }

    function setVALIDBUREAU($CDBUR=null) {
        if ($CDBUR!=null && $CDBUR!='') {
            $data = $this->selectRow('VALIDATEURS', array('CF_CDBUR'=>$CDBUR));
            if ($data!=FALSE) {
                $this->VALID_CDBUR = $data->VALID_CDBUR;
                $this->VALID_BUREAU = $data->VALID_LABEL;
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    
    /**
     * @return null
     */
    public function getDIRNAME() {
        return $this->DIR_NAME;
    }

    /**
     * @param null $DIR_NAME
     */
    public function setDIRNAME($DIR_NAME) {
        $this->DIR_NAME = $DIR_NAME;
    }

    /**
     * @return null
     */
    public function getOPERTYPE() {
        return $this->OPER_TYPE;
    }

    /**
     * @param null $OPER_TYPE
     */
    public function setOPERTYPE($OPER_TYPE) {
        $this->OPER_TYPE = $OPER_TYPE;
    }

    /**
     * @return null
     */
    public function getOPERTK() {
        return $this->OPER_TK;
    }

    /**
     * @param null $OPER_TK
     */
    public function setOPERTK($OPER_TK) {
        $this->OPER_TK = $OPER_TK;
    }

    /**
     * @return null
     */
    public function getDIRCODE() {
        return $this->DIR_CODE;
    }

    /**
     * @param null $DIR_CODE
     */
    public function setDIRCODE($DIR_CODE) {
        $this->DIR_CODE = $DIR_CODE;
    }

    /**
     * @return null
     */
    public function getKEY() {
        return $this->KEY;
    }

    /**
     * @param null $KEY
     */
    public function setKEY($KEY) {
        $this->KEY = $KEY;
    }

    /**
     * @return null
     */
    public function getVALIDCDBUR() {
        return $this->VALID_CDBUR;
    }

    /**
     * @param null $VALID_CDBUR
     */
    public function setVALIDCDBUR($VALID_CDBUR) {
        $this->VALID_CDBUR = $VALID_CDBUR;
    }

    /**
     * @return null
     */
    public function getUSERCODE() {
        return $this->USER_CODE;
    }

    /**
     * @param null $USER_CODE
     */
    public function setUSERCODE($USER_CODE) {
        $this->USER_CODE = $USER_CODE;
    }

    /**
     * @return null|string
     */
    public function getLABEL() {
        return $this->LABEL;
    }

    /**
     * @param null|string $LABEL
     */
    public function setLABEL($LABEL) {
        $this->LABEL = $LABEL;
    }

    function getUSERERROR() {
        return $this->USER_ERROR;
    }

    function setUSERERROR($USER_ERROR) {
        $this->USER_ERROR = $USER_ERROR;
    }

    /**
     * @return null
     */
    public function getEMAIL()
    {
        return $this->EMAIL;
    }

    /**
     * @param null $EMAIL
     */
    public function setEMAIL($EMAIL)
    {
        $this->EMAIL = $EMAIL;
    }



}
