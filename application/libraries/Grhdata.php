<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'grh/Ocidb.php';
include_once 'grh/Codes.php';

class Grhdata extends Ocidb {

    private $data = array();

    public function __construct(){
        parent::init();
        date_default_timezone_set('Africa/Nairobi');
    }

    //ENREGISTRER UN NOUVEAU UTILISATEUR.
    public function registerUser($array_userdata){

        $res_token = Codes::generate_token_code();
        $user_token = Codes::generate_token_code();
        $act_token = Codes::generate_token_code();

        extract($array_userdata);

        $login_data = array(
            'MATRICULE' =>'num*'.$im,
            'EMAIL' =>$mail,
            'DATE_ADD' =>'date*'.$dateAdd,
            'STATUT' =>$statut,
            'USER_TYPE' =>$userType,
            'CD_ACCESS' =>$pass,
            'USERNAME' =>$login,
            'DOMAINE' =>$type,
            'RES_TOKEN' =>$res_token,
            'USR_TOKEN' =>$user_token,
            'ACT_TOKEN' =>$act_token
        );

        $user_datas = array(
            'MATRICULE' =>'num*'.$im,
            'EMAIL' =>$mail,
            'USERNAME'=>$login,
            'NOM'=>$nom,
            'PRENOM'=>$prenom,
            'DIRECTION'=>$direction,
            'SERVICE'=>$service,
            'CD_BUR'=>$cd_bur,
            'BUREAU'=>$centre,
            'ADD_DATE'=>'date*'.$dateAdd,
            'CIN'=>$cin,
            'FONCTION'=>$fonction,
            'TEL'=>$tel,
            'ETABLISSEMENT'=>$etab,
            'GRADE'=>$grade,
            'FIC_ID'=>$fichier
        );

        $this->openTransaction();
        $add[] = $this->addData('GRH_USERS',$login_data);
        $add[] = $this->addData('PERSONNEL',$user_datas);
        $add[] = $this->addDefaultApps($login);

        if (!in_array(false,$add)) {
            $this->commit();
            $this->endTransaction();
            return true;
        }else{
            $this->rollback();
            $this->endTransaction();
            return false;
        }
    }


    //AJOUT APPLICATION PAR DEFAUT.
    public function addDefaultApps($username){
        $today = date('d/m/Y H:i:s');
        $defaults = $this->defaultApps();

        foreach($defaults as $key => $appli){
            $apps[] = array('USERNAME'=>$username,'CD_APP'=>$appli->CD_APP,'PROFILE'=>$appli->DEFAULT_PRIV,'ACTIF'=>'num*1','VALIDATEUR'=>'num*111111','ROLE_1'=>'0','DATE_AJOUT'=>'date*'.$today);
        }

        $this->openTransaction();

        foreach($apps as $value){
            $add[]=$this->addData('USER_APP',$value);
        }

        if (!in_array(false,$add)) {
            $this->commit();
            $this->endTransaction();
            return true;
        }else{
            $this->rollback();
            $this->endTransaction();
            return false;
        }
    }


    //LISTE DIRECTIONS.
    public function directions(){
         $list = $this->findAll('DIRECTIONS');
         return $list;
    }


    //Listes des Nom des services de la dgi. 
    public function services(){
        $list = $this->findAll('SERVICES');
        return $list;
    }


    //Liste des services d'une direction.
    public function serviceDirection($dir){
        $list = $this->findAll('SERVICES',array('DIR'=>$dir));
        return $list;
    }


    //GENERER UN MOT DE PASSE.
    public function generatePaswd($size=null)
    {
        if ($size==null || $size<=6){
        	$length = 6;
        }else{
            $length = $size;
        }
        $string =substr(str_shuffle(str_repeat('0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN',40)),60,$length);
        return $string;
    }


    //VERIFIER LE TOKEN D'ACTIVATION.
    public function checkact($user, $token)
    {
        $check = $this->countRows('GRH_USERS','USERNAME',array('USERNAME'=>$user,'ACT_TOKEN'=>$token));
        if ($check==1){
        	return true;
        }else{
            return false;
        }
    }


    //VERIFIER LE TOKEN D'ACTIVATION.
    public function checkRestk($user, $token)
    {
        $check = $this->countRows('GRH_USERS','USERNAME',array('USERNAME'=>$user,'RES_TOKEN'=>$token));
        if ($check==1){
        	return true;
        }else{
            return false;
        }
    }


    /**
     * ACTIVER UN NOUVEAU UTILISATEUR.
     * @param  String $user
     * @param  String $token
     * @return Bool  $update
     */
    public function activateUser($user,$token)
    {
        $act_token = Codes::generate_token_code();

        $update = $this->updateData('GRH_USERS',array('ACT_TOKEN'=>$act_token,'STATUT'=>'actif'),array('USERNAME'=>$user,'ACT_TOKEN'=>$token));
        return $update;
    }


    public function prepare_reset($cin)
    {
        $sql = "SELECT COUNT(CIN) AS NOMBRE FROM USERS_INFOS WHERE CIN =:cin";
        $params = array(':cin' => $cin);
        $nbr = $this->do_query($sql, $params)->NOMBRE;

        if ($nbr >= 1) {
            $statut = true;

        } else {
            $statut = false;
        }

        return $statut;
        //return $nbr;
    }


    public function updatePass($datas)
    {
        $user = $datas['user'];
        $token = $datas['tk'];
        $pass = hash('sha256',$datas['pass']);
        $res_token = Codes::generate_token_code();

        $this->openTransaction();
        $update[] = $this->updateData('GRH_USERS',array('CD_ACCESS'=>$pass,'RES_TOKEN'=>$res_token,'STATUT'=>'actif'),array('USERNAME'=>$user,'RES_TOKEN'=>$token));
        //$update[] = $this->updateData('GRH_USERS',array('RES_TOKEN'=>$res_token),array('USERNAME'=>$user,'RES_TOKEN'=>$token));

        if (in_array(false,$update)){
        	$this->rollback();
            $this->endTransaction();
            return false;
        }else{
            $this->commit();
            $this->endTransaction();
            return true;
        }
    }


    public function rollbackRegister($user,$email)
    {
        $del[] = $this->deleteData('GRH_USERS',array('USERNAME'=>$user,'EMAIL'=>$email));
        $del[] = $this->deleteData('PERSONNEL',array('USERNAME'=>$user,'EMAIL'=>$email));
        $del[] = $this->deleteData('USER_APP',array('USERNAME'=>$user));

        if (in_array(false,$del)){
        	return false;
        }else{
            return true;
        }
   }


    public function userApps($user)
    {
        $type = $this->userDomain($user);

        if ($type =='Interne'){
        	$sql = "SELECT CD_APP AS CODE,APP_NOM AS NOM,SLUG,STATUS FROM APP_LIST WHERE  nvl(DEFAULT_APP,'no')<>'yes'";
        }else{
            $sql = "SELECT CD_APP AS CODE,APP_NOM AS NOM,SLUG,STATUS FROM APP_LIST";
        }

        $listes = $this->do_select($sql,$param=null);
        //var_dump($listes);
        //die();
        return $listes;
    }


    private function userDomain($user)
    {
        $domain = $this->selectRow('GRH_USERS',array('USERNAME'=>$user));
        return $domain->DOMAINE;
    }


    public function adminliste(){
        $result = $this->findAll('APPS_ADMIN');
        return $result;
    }

    public function updateUserStatut($user)
    {   $sql = "UPDATE GRH_USERS SET APP_VALID =:statut WHERE USERNAME =:username";
        $params = array(':statut'=>'PENDING',':username'=>$user);

        $update = $this->do_updateQuery($sql,$params);
        return $update;
    }

    public function defaultApps()
    {
        $sql = "SELECT CD_APP,APP_NOM,DEFAULT_PRIV FROM APP_LIST WHERE DEFAULT_APP='yes'";
        $liste = $this->do_select($sql);
        return $liste;
    }


}