<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once 'grh/Ocidb.php';
include_once 'grh/Codes.php';

class Apidata extends Ocidb
{
  protected $data = array();
  
  
  function __construct()
  {
    parent::init();
    date_default_timezone_set('Africa/Nairobi');
  }
 
  public function listeApp()
  {
    return $this->findAll('APP_LIST');
  }
}