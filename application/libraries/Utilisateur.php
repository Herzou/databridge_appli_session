<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'grh/Ocidb.php';
include_once 'grh/Codes.php';

class Utilisateur extends Ocidb {

      private $FONCTION = null;
      private $USER_TYPE = null;
      private $ADD_DATE = null;
      private $STATUT = null;
      private $MATRICULE = null;
      private $NOM = null;
      private $PRENOM = null;
      private $EMAIL = null;
      private $DIRECTION = null;
      private $SERVICE = null;
      private $CD_BUR = null;
      private $PHOTO = null;
      private $FIC_ID = null;
      private $SESS_TOKEN = null;
      private $ACT_TOKEN = null;
      private $RES_TOKEN = null;
      private $USERNAME = null;
      private $GRADE= null;
      private $DOMAINE= null;
      private $TEL= null;
      private $CIN= null;
      private $ETABLISSEMENT = null;

    public function __construct()
    {
        parent::init();
    }

    /**
     * @param null $MATRICULE
     */
    public function setMATRICULE($MATRICULE)
    {
        $infos = $this->selectRow('USERS_INFOS',array('MATRICULE'=>$MATRICULE));
        if ($infos!=false){
        	foreach($infos as $key => $info){
        	  $this->$key = $info;
        	}
            return true;
        }else{
            return false;

        }
    }

    /**
     * @param null $USERNAME
     */
    public function setUSERNAME($USERNAME)
    {
        $infos = $this->selectRow('USERS_INFOS',array('USERNAME'=>$USERNAME));
        if ($infos!=false){
            foreach($infos as $key => $info){
                $this->$key = $info;
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param null $EMAIL
     */
    public function setEMAIL($EMAIL)
    {
        $infos = $this->selectRow('USERS_INFOS',array('EMAIL'=>$EMAIL));
        if ($infos!=false){
            foreach($infos as $key => $info){
                $this->$key = $info;
            }
            return true;
        }else{
            return false;

        }
    }

    /**
     * @param null $CIN
     */
    public function setCIN($CIN)
    {
        $infos = $this->selectRow('USERS_INFOS',array('CIN'=>$CIN));
        if ($infos!=false){
            foreach($infos as $key => $info){
                $this->$key = $info;
            }
            return true;
        }else{
            return false;

        }
    }

    /**
     * @return null
     */
    public function getCIN()
    {
        return $this->CIN;
    }

    /**
     * @return null
     */
    public function getETABLISSEMENT()
    {
        return $this->ETABLISSEMENT;
    }



    /**
     * @return null
     */
    public function getTEL()
    {
        return $this->TEL;
    }



    /**
     * @return null
     */
    public function getUSERNAME()
    {
        return $this->USERNAME;
    }

    /**
     * @return null
     */
    public function getGRADE()
    {
        return $this->GRADE;
    }

    /**
     * @return null
     */
    public function getDOMAINE()
    {
        return $this->DOMAINE;
    }



    /**
     * @return null
     */
    public function getFONCTION()
    {
        return $this->FONCTION;
    }

    /**
     * @return null
     */
    public function getUSERTYPE()
    {
        return $this->USER_TYPE;
    }

    /**
     * @return null
     */
    public function getADDDATE()
    {
        return $this->ADD_DATE;
    }

    /**
     * @return null
     */
    public function getSTATUT()
    {
        return $this->STATUT;
    }

    /**
     * @return null
     */
    public function getMATRICULE()
    {
        return $this->MATRICULE;
    }

    /**
     * @return null
     */
    public function getNOM()
    {
        return $this->NOM;
    }

    /**
     * @return null
     */
    public function getPRENOM()
    {
        return $this->PRENOM;
    }

    /**
     * @return null
     */
    public function getEMAIL()
    {
        return $this->EMAIL;
    }

    /**
     * @return null
     */
    public function getDIRECTION()
    {
        return $this->DIRECTION;
    }

    /**
     * @return null
     */
    public function getSERVICE()
    {
        return $this->SERVICE;
    }

    /**
     * @return null
     */
    public function getCDBUR()
    {
        return $this->CD_BUR;
    }

    /**
     * @return null
     */
    public function getPHOTO()
    {
        return $this->PHOTO;
    }

    /**
     * @return null
     */
    public function getFICID()
    {
        return $this->FIC_ID;
    }

    /**
     * @return null
     */
    public function getSESSTOKEN()
    {
        return $this->SESS_TOKEN;
    }

    /**
     * @return null
     */
    public function getACTTOKEN()
    {
        return $this->ACT_TOKEN;
    }

    /**
     * @return null
     */
    public function getRESTOKEN()
    {
        return $this->RES_TOKEN;
    }


}