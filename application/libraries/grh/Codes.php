<?php

class Codes {

    //Générer la réference d'un texte.
    public static function serial() {
        $str1 = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZW', 100)), 40, 6);
        $str2 = substr(str_shuffle(str_repeat('123STVU4567IJKLMN890XABCDEFGHOPQRYZW', 50)), 80, 6);
        $serial = $str1 . '-' . $str2;

        return $serial;
    }

    //Générer les codes pour chaque type de confidentialité
    public static function gen_codes($droit) {

        if ($droit == 'Privée') {
            $secret = 'DHZ8FO4KWE7K';
        } elseif ($droit == 'DGI') {
            $secret = 'O5BLR0E2T7Y7';
        } else {
            $secret = 'QR5AECEP40TO';
        }

        return $secret;
    }

    //Générer les codes pour chaque type d'operateur
    public static function codes_types($types) {

        if ($types == 'Admin' || $types == 'Oper') {
            $res = 'DHZ8FO4KWE7K';
        } elseif ($types == 'Consult') {
            $res = 'O5BLR0E2T7Y7';
        } else {
            $res = 'QR5AECEP40TO';
        }

        return $res;
    }

    //Extraire le Type d'operateur
    public static function extract_codes_types($code_types) {
        if ($code_types == 'DHZ8FO4KWE7K') {
            $level = 1;
        } elseif ($code_types == 'O5BLR0E2T7Y7') {
            $level = 2;
        } elseif ($code_types == 'QR5AECEP40TO') {
            $level = 3;
        }
        return $level;
    }

    //Générer les codes de sécurité pour les liens vers un Texte.
    public static function security_code($droit) {

        $secret = self::gen_codes($droit);

        $str1 = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZWazertyuiopqsdfghjklmwxcvbn', 100)), 100, 50);
        $str2 = substr(str_shuffle(str_repeat('123STVU4567IJKLMN890XABCDEFGHOPQRYZWazertyuiopqsdfghjklmwxcvbn', 80)), 150, 50);
        $security = $str1 . $secret . $str2;

        return $security;
    }

    //Extraire la code de sécurité sur les liens des textes.
    public static function extract_security_code($token) {

        $res = substr($token, 50, 12);

        return $res;
    }

    // Générer les Token opérateurs pour les Utilisateurs
    public static function generate_token_code() {
        $str1 = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZWazertyuiopqsdfghjklmwxcvbn', 100)), 100, 60);
        $str2 = substr(str_shuffle(str_repeat('123STVU4567IJKLMN890XABCDEFGHOPQRYZWazertyuiopqsdfghjklmwxcvbn', 80)), 150, 60);
        $security = $str1. $str2;

        return $security;
    }


    // Générer un token
    public static function generate_token() {
        $str = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZWazertyuiopqsdfghjklmwxcvbn', 100)), 100, 50);
        return $str;
    }

    //Générer les mot de passes pour les Utilisateurs
    public static function generate_pass($length) {
        $str1 = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZW', 100)), 73, $length);
        return $str1;
    }

    public static function generate_code() {
        $ncu = substr(str_shuffle(str_repeat('1234567890', 20)), 40, 14);
        return $ncu;
    }

}
