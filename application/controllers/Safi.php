<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
class Safi extends REST_Controller
{

    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Key_mod', 'apikey');
        $this->load->model('api/Safi_mod','safi');
    }


    public function users_get()
    {
        $cdbur = $this->get('cdbur');
        $appnom= $this->get('app');
        $key = $this->get('key');

        if ($this->get('key') && $this->apikey->key($key) == true) {
            $this->data['userinfo'] = $this->safi->usersList($cdbur,$appnom);
            $this->data['error'] = '';
            $this->data['status'] = true;
        } else {
            $this->data['userinfo'] = array();
            $this->data['error'] = 'KEY_ERROR : La clé de sécurité est invalide';
            $this->data['status'] = false;
        }
        $this->response($this->data,200);
    }

    public function userinfo_get()
    {
        $infs = array('USERNAME'=>'','EMAIL'=>'','MATRICULE'=>'','NOM'=>'','PRENOM'=>'','MPASS'=>'','APP_NOM'=>'',
                      'ACTIF'=>'','CD_BUR'=>'','PROFILE'=>'','ROLE_1'=>'','ROLE_2'=>'','SERVICE'=>'');

            $im= $this->get('im');
            $key = $this->get('key');

        if ($this->get('key') && $this->apikey->key($key) == true) {
            $infos = $this->safi->userInfo($im);
            if (!empty($infos)){
                $this->data['userinfo'] = $infos;
                $this->data['error'] = '';
                $this->data['status'] = true;
                $this->data['access'] = 'O';
            }else{
                $this->data['userinfo'] = array($infs);
                $this->data['error'] = 'Cet utilisateur n\'a pas d\'autorisatuin sur SAFI';
                $this->data['status'] = true;
                $this->data['access'] = 'N';
            }
        } else {
            $this->data['userinfo'] = array();
            $this->data['error'] = 'KEY_ERROR : La clé de sécurité est invalide';
            $this->data['status'] = false;
            $this->data['access'] = 'F';
        }
        $this->response($this->data,200);
    }
}