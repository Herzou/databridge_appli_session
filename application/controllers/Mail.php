<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Mail extends REST_Controller
{
    protected $data = array();
    protected $error = '';


    function __construct()
    {
        parent::__construct();
        $this->load->model('Mailing','mailer');
        date_default_timezone_set('Africa/Nairobi');
    }

    //Envoyer un email
    public function envoyer_put(){

        $params = array(
            'from'=>$this->put('de'),
            'name'=>$this->put('nom'),
            'to'=> $this->put('a'),
            'subject'=>$this->put('subject')
        );
        $message = $this->put('message');

        $send = $this->mailer->sendEmail($params,$message);

        $this->data['statut'] = $send;
        $this->data['message'] = $this->mailer->getMessage();

        $this->response($this->data,200);
    }

}