<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Test extends REST_Controller
{
    protected $data = array();


    function __construct()
    {
        parent::__construct();
        $this->load->model('test/Test_mod','testmod');
    }

    public function index()
    {
        if (!empty($this->input->post())) {
            $res = $this->ads->addApi($this->input->post());
            //var_dump($this->input->post(),$res);
        }
        $this->data['pass'] = hash('sha256','159753');
        $this->load->view('test_view', $this->data);
    }

    public function stat_post()
    {
        $user = $this->post('user');
        $pass = $this->post('pass');
        $this->data['user'] = $this->testmod->selectUser($user,$pass);
        $this->response($this->data,200);
    }
}