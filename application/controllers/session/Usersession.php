<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Usersession extends REST_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();


        $this->load->model('session/session_mod','mysession');
    }

    // verifier la session d'un utilisateur.
    public function check_get()
    {
        $token = $this->get('token');

        if ($token) {
            $this->mysession->getSession($token);
            $sessionInfos = $this->mysession->getMysession();

            $this->data['status'] = true;
            $this->data['usr_name'] = $this->mysession->getUsername();
            $this->data['usr_session'] = $sessionInfos;
            $this->data['error'] = $this->mysession->getMessage();
        } else {

            $this->data['status'] = false;
            $this->data['usr_name'] = '';
            $this->data['usr_session'] = array();
            $this->data['error'] = 'erreur de la session';
        }
        $this->response($this->data, 200);
	}
}
