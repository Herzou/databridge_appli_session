<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Ar extends REST_Controller
{
    protected $data = array();
    protected $error = '';


    function __construct()
    {
        parent::__construct();
        $this->load->library('Ardata',null,'ardb');
        date_default_timezone_set('Africa/Nairobi');
    }


    public function analyserisque_get(){
        $nif  = $this->get('nif');
        $annee = $this->get('annee');
        $data = $this->ardb->analiseRisk($nif,$annee);
        //var_dump($data);

        if(isset($nif)  && !empty($data)){
            $this->data['ar_risk'] = $data;
            $this->data['ar_statut'] = true;
        }else{
            $this->data['ar_risk'] = array();
            $this->data['ar_statut'] = false;
            $this->data['ar_error'] = 'aucun resultat trouvé';
        }
        $this->response($this->data,200);
    }



    public function analyserisques_get(){
        $nif  = $this->get('nif');
        $data = $this->ardb->analiseRisque($nif);
        //var_dump($data);

        if(isset($nif)  && !empty($data)){
            $this->data['ar_risk'] = $data;
            $this->data['ar_statut'] = true;
        }else{
            $this->data['ar_risk'] = array();
            $this->data['ar_statut'] = false;
            $this->data['ar_error'] = 'aucun resultat trouvé';
        }
        $this->response($this->data,200);
    }
    

    public function indicateur_get(){
        $nif  = $this->get('nif');
        $annee = $this->get('annee');
        $data = $this->ardb->arIndicateur($nif,$annee);
        if(isset($nif)  && !empty($data)){
            $this->data['ar_risk'] = $data;
            $this->data['ar_statut'] = true;
        }else{
            $this->data['ar_risk'] = array();
            $this->data['ar_statut'] = false;
            $this->data['ar_error'] = 'aucun resultat trouvé';
        }
        $this->response($this->data,200);
    }
}

