<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Dossier extends REST_Controller
{
    protected $data = array('message' => null, 'statut' => false);


    function __construct()
    {
        parent::__construct();
        $this->load->library('Sqdata', null, 'sq');
    }

    public function dossiers_get()
    {
        $dos = $this->sq->dossiersList();
        if ($dos == false) {
            $this->data['message'] = 'Aucun resultat trouv�';
            $this->data['statut'] = false;
        } else {
            $this->data['response'] = $dos;
            $this->data['statut'] = true;
        }
        $this->response($this->data, 200);
    }


    public function dossier_get()
    {

        $dos = $this->sq->dossier($this->get('ncu'));
        if ($dos == false) {
            $this->data['message'] = 'Aucun resultat trouv�';
            $this->data['statut'] = false;
        } else {
            $this->data['response'] = $dos;
            $this->data['statut'] = true;
        }
        $this->response($this->data, 200);
    }


    public function controls_get()
    {

    }


    public function addcontrol_put()
    {
        $datas['nif'] = $this->put('nif');
        $datas['cdbur'] = $this->put('cb');

        $control = $this->sq->adddossControl($datas);
        if ($control == false) {
            $this->data['message'] = 'Impossibe d\'ajouter le donn�e';
            $this->data['statut'] = false;
        } else {
            $this->data['response'] = null;
            $this->data['statut'] = true;
            $this->data['message'] = 'Donn�e ajout�e avec succ�s';
        }
            $result = $this->data;
        $this->response($result, 200);
    }
}