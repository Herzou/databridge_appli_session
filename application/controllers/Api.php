<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Api extends REST_Controller
{
    protected $data = array();


    function __construct()
    {
        parent::__construct();
        $this->load->model('api/App_mod', 'appli');
    }

    /**
     *LISTE DES APPLICATIONS
     */
    public function applications_get()
    {
        $apps = $this->appli->ApplicationsList();
        $this->data['status'] = true;
        $this->data['liste'] = $apps;
        $this->data['error'] = '';

        $this->response($this->data, 200);
    }


    /**
     * DETAILS SUR UNE APPLICATION.
     */
    public function application_get()
    {
        $code = $this->get('code');
        $apps = $this->appli->ApplicationInfo($code);
        $this->data['status'] = true;
        $this->data['liste'] = $apps;
        $this->data['error'] = '';

        $this->response($this->data, 200);
    }


    /**
     *SUPPRIMER UNE APPLICATION.
     */
    public function application_delete()
    {
        $id = (int)$this->delete('id');
        $delete = $this->appli->ApplicationDelete($id);
        var_dump($delete);
        if ($delete == true) {
            $this->data['status'] = true;
            $this->data['message'] = 'L\'enregistrement ' . $id . ' à été supprimé avec succès';
            $this->data['error'] = null;
        } else {
            $this->data['status'] = false;
            $this->data['message'] = 'Impossible de supprimer l\'enregistrement ' . $id;
            $this->data['error'] = 'ERREUR_EXECUTION impossible d\'executer la requête';
        }

        $this->response($this->data, 200);
    }


    /**
     *AJOUTER UNE NOUVELLE APPLICATION.
     */
    public function application_put()
    {
        $datas = $this->put();
        $add = $this->appli->ApplicationAdd($datas);
        if ($add == TRUE) {
            $this->data['status'] = true;
            $this->data['message'] = 'Nouvelle ligne de donnée crée.';
            $this->data['error'] = '';
        } else {
            $this->data['status'] = $add;
            $this->data['message'] = 'L\'ajout de la nouvelle application à échoué.';
            $this->data['error'] = 'DATA_ERROR Données incompatibles ou vide (NULL).';
        }

        $this->response($this->data, 200);
    }


    /**
     * METTRE A JOUR UNE APPLICATION.
     */
    public function application_post()
    {
        $datas = $this->post();
        $add = $this->appli->ApplicationUpdate($datas);
        if ($add == TRUE) {
            $this->data['status'] = true;
            $this->data['message'] = 'La ligne à été modifié avec succès.';
            $this->data['error'] = '';
        } else {
            $this->data['status'] = $add;
            $this->data['message'] = 'La modification des informations à échouée';
            $this->data['error'] = 'DATA_ERROR Données incompatibles ou vide (NULL).';
        }

        $this->response($this->data, 200);
    }


    public function code_get()
    {
        $this->data['data'] = $this->appli->maximum();
        $this->data['status'] = true;

        $this->response($this->data,200);
    }
}
