<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Grh extends REST_Controller
{
    private $data = array();
    private $error = '';
//ok!

    function __construct()
    {
        parent::__construct();
        $this->load->library('Users', null, 'user');
        $this->load->library('Grhdata', null, 'grhdb');
        $this->load->model('User_mod', 'grhuser');
        $this->load->model('api/Api_mod', 'apis');
        $this->load->model('api/Service_mod', 'serv');
        $this->load->model('Mailing','mailer');
		$this->load->model('applications/Apps_mod', 'apps');
        date_default_timezone_set('Africa/Nairobi');
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    //PAGE D'ACCUEIL
    public function index()
    {

    }

    //API DE LOGIN DES APPLICATIONS.
    public function login_post()
    {
        $user = $this->post('user');
        $passwd = $this->post('pass');
        $log = $this->user->connect_user($user, $passwd);
        //$log['statut'] = $test;

        if ($log['statut'] == true) {
            $this->data['usr_statut'] = 1;
            $this->data['status'] = true;
            $this->data['usr_infos'] = $log['data'];
            $this->data['error'] = $this->user->getUSERERROR();
        } else {
            $this->data['usr_statut'] = 0;
            $this->data['status'] = true;
            $this->data['usr_infos'] = $log['data'];
            $this->data['error'] = $this->user->getUSERERROR();
        }
        $this->response($this->data, 200);
    }

    //LISTE DES APPLICATIONS D'UN UTILISATEUR
    public function userapp_get()
    {
        $user = $this->get('user');
        $token = $this->get('token');
        $check = $this->user->validateToken($user, $token);
        if ($check == true) {
            $apps = $this->apps->userApps($user);
            $this->data['usr_apps'] = $apps;
            $this->data['usr_statut'] = true;
        } else {
            $this->data['usr_error'] = 'Autorisation invalide!';
            $this->data['usr_statut'] = false;
            $this->data['usr_apps'] = array();
        }
        $this->response($this->data['usr_apps'], 200);
    }

    //VERIFIER LA SESSION D'UN UTILISATEUR
    public function usersession_get()
    {
        $sessionId = $this->get('id');
        $check = $this->user->validateSession($sessionId);

        if ($check == true) {
            $user = $this->user->getUSERNAME();
            $userinfo = $this->user->userInfos($user, $sessionId);

            if ($userinfo != false) {
                //$apps = $this->user->applist($user);
                $apps = $this->apps->userApps($user);
                $this->data['usr_apps'] = $apps;
                $this->data['usr_infos'] = $userinfo;
                $this->data['usr_statut'] = true;
                $this->data['usr_name'] = $user;
            } else {
                $this->data['usr_error'] = $this->user->getUSERERROR();
                $this->data['usr_statut'] = false;
                $this->data['usr_apps'] = array();
                $this->data['usr_infos'] = array();
            }
        } else {
            $this->data['usr_error'] = $this->user->getUSERERROR();
            $this->data['usr_statut'] = $check;
            $this->data['usr_apps'] = array();
            $this->data['usr_infos'] = array();
        }
        $this->response($this->data, 200);
    }

    //FERMER LA SESSION
    public function logout_get()
    {
        $sessionId = $this->get('id'); //'id' = ID de la session à fermer
        $logout = $this->user->endsession($sessionId);
        if ($logout == true) {
            $this->data['usr_statut'] = true;
            $this->data['usr_error'] = 'La session à  été férmé aves success';
        } else {
            $this->data['usr_statut'] = false;
            $this->data['usr_error'] = 'Erreur de connexion à la base de donnée.';
        }
        $this->response($this->data, 200);
    }

    //INSCRIPTION D'UN UTILISATEUR
    public function register_put()
    {
        $datasPut = $this->put();

        $add = $this->grhuser->registerUser($datasPut);
        //$add=true;
        if ($add == true) {
            $this->data['data_statut'] = true;
            $this->data['data_msg'] = $this->grhuser->getMessage();
            $this->data['login'] = $this->put('login');
            $this->data['password'] = $this->grhuser->getPassword();

        } else {
            $this->data['data_statut'] = false;
            $this->data['data_error'] = $this->grhuser->getMessage();
            $this->data['login'] = $this->put('login');
            $this->data['password'] = null;
        }

        $this->response($this->data, 200);
    }

    //LISTE DES DIRECTIONS A LA DGI
    public function directions_get()
    {
        $list = $this->grhdb->directions();
        if (!empty($list)) {
            $this->data['data_statut'] = true;
            $this->data['data_content'] = $list;
        } else {
            $this->data['data_statut'] = false;
            $this->data['data_content'] = array();
            $this->data['data_error'] = 'Le résultat est vide ou inéxistant';
        }
        $this->response($this->data, 200);
    }

    //LISTES DES SERVICES A LA DGI
    public function services_get()
    {
        $list = $this->grhdb->services();
        if (!empty($list)) {
            $this->data['data_statut'] = true;
            $this->data['data_content'] = $list;
        } else {
            $this->data['data_statut'] = false;
            $this->data['data_content'] = array();
            $this->data['data_error'] = 'Le résultat est vide ou inéxistant';
        }
        $this->response($this->data, 200);
    }


    //LISTE DES SERVICES D'UNE DIRECTION DONNEE
    public function service_get()
    {
        $dir = $this->get('dir');
        $list = $this->grhdb->serviceDirection($dir);
        if (!empty($list)) {
            $this->data['data_statut'] = true;
            $this->data['data_content'] = $list;
        } else {
            $this->data['data_statut'] = false;
            $this->data['data_content'] = array();
            $this->data['data_error'] = 'Le résultat est vide ou inéxistant';
        }
        $this->response($this->data, 200);
    }


    //VERIFIER ET ACTIVER LE TOKEN D'ACTIVATION
    public function checkactivetoken_get()
    {
         $datas = $this->get();
         $check = $this->grhuser->checkAct($datas);
         if ($check==true){
         	$this->data['statut'] = true;
         	$this->data['message'] = $this->grhuser->getMessage();
         }else{
             $this->data['statut'] = false;
             $this->data['message'] = $this->grhuser->getMessage();
         }

         $this->response($this->data,200);
    }


    //VERIFIER ET ACTIVER LE TOKEN D'ACTIVATION
    public function checkresettoken_post()
    {
         $datas = $this->post();
         $check = $this->grhuser->checkRestk($datas);
         if ($check==true){
         	$this->data['statut'] = true;
         	$this->data['message'] = $this->grhuser->getMessage();
         }else{
             $this->data['statut'] = false;
             $this->data['message'] = $this->grhuser->getMessage();
         }

         $this->response($this->data,200);
    }

    
    //VERIFIER UN NOM D'UTLISATEUR S'IL EXISTE.
    public function checkuser_post()
    {
        $username = $this->post('user');
        $check = $this->grhuser->checkUser($username);
        $this->data['statut'] = $check;
        $this->data['response'] = $this->grhuser->getMessage();

        $this->response($this->data,200);
    }


    //VERIFIER UN EMAIL S'IL EXISTE.
    public function unikmail_post()
    {
        $email = $this->post('email');
        $check = $this->grhuser->unikMail($email);
        $this->data['statut'] = $check;
        $this->data['response'] = $this->grhuser->getMessage();

        $this->response($this->data,200);
    }


    //VERIFIER UN CIN S'IL EXISTE.
    public function unikcin_post()
    {
        $cin = $this->post('cin');
        $check = $this->grhuser->unikCin($cin);
        $this->data['statut'] = $check;
        $this->data['response'] = $this->grhuser->getMessage();

        $this->response($this->data,200);
    }


    //VALIDER SI UN CIN EXISTE
    public function validatecin_post(){
        $cin = $this->post('cin');
        $email = $this->post('email');


        $this->data['statut'] = $this->grhuser->validateReset($cin,$email);
        $this->data['data'] = $this->grhuser->getData();
        $this->data['message'] = $this->grhuser->getMessage();

        $this->response($this->data,200);
    }


    public function validateapps_post()
    {
        $user = $this->post('user');
        //$email = $this->grhuser->userInfos($user)->EMAIL;
        $datas['apps'] = $this->grhuser->userApplis($user);
        $datas['email'] = 'herzou.net2job@gmail.com';
        $datas['user'] = $user;
        $valid = $this->mailer->validationEmail($datas);
        if ($valid == true) {
            $this->data['data_statut'] = true;
            $this->data['data_msg'] = 'un email de notification est envoyé';
            $this->data['infos'] = $datas;
        } else {
            $this->data['infos'] = $datas;
            $this->data['data_statut'] = false;
            $this->data['data_msg'] = 'erreur d\'envoi de l\'email de validation' ;
        }

        $this->response($this->data, 200);
    }


    //LISTE DES APPLICATIONS SELON LE DOMAINE DE L'UTILISATEUR
    public function applist_post()
    {
        $user = $this->post('user');
        $apps = $this->grhuser->applistType($user);

        $this->data['liste'] = $apps;
        $this->data['statut'] = true;

       $this->response($this->data,200);
    }


    //ENREGISTRER UN NOUVEAU MOT DE PASSE.
    public function password_post(){
        $data = $this->post();
        $check = $this->grhuser->checkRestk($data);
        if ($check==true) {
            $update = $this->grhuser->updatePass($data);
            $this->data['statut'] = $update;
            if (!$update){
                $this->data['message'] = '<div class="alert alert-danger">Impossible de mettre à jour le mot de passe. Reéssayer encore, si non contacter l\'administrateur du siteweb.</div>';
            }else{
                $this->data['message'] = '<div class="alert alert-success">Votre mot de passe a été modifié avec succès. Cliquez le lien suivant pour se connecter. <em><a href="'.PORTAL_HOME.'"> SE CONNECTER</a></em></div>';
            }
        }else{
            $this->data['statut'] = false;
            $this->data['message'] = '<div class="alert alert-danger">La page que vous essayez d\'afficher  contient une ou  des paramètres invalides.</div>';
        }

        $this->response($this->data,200);
    }
    


    //ENVOYER UNE DEMANDE d'ACCES AU APPLICATIONS.
    public function demande_post()
    {
        $user = $this->post('user');
        $datas = $this->post();

        $send = $this->grhuser->demandeApp($datas);


        //$send = true;
        $this->data['user'] = $user;
        $this->data['statut'] = $send;


        if ($send==true){
            $this->data['message'] = '<div class="alert alert-success">Votre demande d\'accès aux applications à été envoyée avec succès. Elle est maintenant en attente de validation par l\'administrateur du siteweb.'.
            'Veuillez reconsulter votre compte DGI-Portal dans le prochain <strong>24 ou 48 heures</strong><br><br>'.
            'Cliquer sur le lien suivant pour se connecter à votre compte "<a href="'.PORTAL_HOME.'" class="bold">SE CONNECTER</a>"</div>';
        }else{
            $this->data['message'] = '<div class="alert alert-danger">Echec de l\'envoi de votre demande d\'accès aux applications. Veuillez reessayer en cliquant sur le lien '.
            '<a href="'.PORTAL_HOME.'/register/demande/'.$user.'" class="bold"> ICI </a> ou <a href="'.PORTAL_HOME.'/register/demande/'.$user.'" class="bold"> REESSAYER </a>';
            //$this->data['message'] = 'Erreur';
        }


        $this->response($this->data,200);
    }
    
    
    //OBTENIR LES iNFOS D'UN UTILISATEUR
    public function userinfo_get()
    {
        $id = $this->get('im');
        $infos = $this->apis->userInfos($id);
        if (!empty($infos)) {
            $data = array('status'=>true,'datas'=>$infos,'error'=>'');
        } else {
            $data = array('status'=>false,'datas'=>array(),'error'=>'Cet objet n\'existe pas dans la base de donnée');
        }

        $this->response($data,200);
    }


    //Info utilisateur  a partir  username (get).
    public function userdetails_get()
    {
        $user = $this->get('user');
        $infos = $this->apis->userDetails($user);
        if (!empty($infos)) {
            $data = array('status'=>true,'datas'=>$infos,'error'=>'');
        } else {
            $data = array('status'=>false,'datas'=>array(),'error'=>'Cet objet n\'existe pas dans la base de donnée');
        }

        $this->response($data,200);
    }


    //Liste matricules
    public function immatri_get()
    {
        $im = $this->get('char');
        $infos = $this->apis->userListe($im);
        if (!empty($infos)) {
            $data = array('status'=>true,'datas'=>$infos,'error'=>'');
        } else {
            $data = array('status'=>false,'datas'=>array(),'error'=>'Cet objet n\'existe pas dans la base de donnée');
        }

        $this->response($data,200);
    }


    public function centrefiscal_get()
    {
        $cdbur= $this->get('cdbur');
        $infos = $this->serv->burInfo($cdbur);
        if (!empty($infos)) {
            $data = array('status'=>true,'datas'=>$infos,'error'=>'');
        } else {
            $data = array('status'=>false,'datas'=>array(),'error'=>'Cet objet n\'existe pas dans la base de donnée');
        }

        $this->response($data,200);
    }
}
