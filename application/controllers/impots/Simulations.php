<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * SERVICES DE SIMULATION DES CALCULS D'IMPOTS SUR WWW.IMPOTS.MG.
 */
require APPPATH . 'libraries/REST_Controller.php';

class Simulations extends REST_Controller
{
    protected $data = array();


    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Africa/Nairobi');

        $this->load->model('Key_mod', 'apikey');
        $this->load->model('simulations/impots_mod', 'imps');
    }


    public function tauxirsa_get()
    {
        $irsa = $this->get('irsa');
       // $key = $this->get('key');


        if ($irsa)
        {
            $taux = $this->imps->irsaTaux($irsa);
            $this->data['status'] = true;
            $this->data['taux'] = $taux;
            $this->data['error'] = '';
        }else{
            $this->data['status'] = false;
            $this->data['taux'] = null;
            $this->data['error'] = 'La valeur de l\'IRSA ne doit pas  être null';
        }

        $this->response($this->data,200);

    }

    public function calculirsa_post()
    {
        $data['sb'] = $this->post('sb');
        $data['accessoires']= $this->post('accessoires');
        $data['cnaps']= $this->post('cnaps');
        $data['avantages']= $this->post('avantages');
        $data['cotisation']= $this->post('cotisation');
        $data['deductions']= $this->post('deductions');
       // $key = $this->get('key');


        if ($data['sb'])
        {
            $irsa= $this->imps->calculIrsa($data);
            $this->data['status'] = true;
            $this->data['mnt'] = $this->imps->getMontant();
            $this->data['taux'] = $this->imps->getTauxIrsa();
            $this->data['irsa'] = $irsa;
            $this->data['error'] = '';
        }else{
            $this->data['status'] = false;
            $this->data['mnt'] = null;
            $this->data['taux'] = null;
            $this->data['irsa'] = null;
            $this->data['error'] = 'La valeur de l\'IRSA ne doit pas  être null';
        }

        $this->response($this->data,200);

    }

}