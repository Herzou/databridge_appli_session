<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
class Apitest extends REST_Controller
{
  protected $data = array();
  
  
  function __construct()
  {
      parent::__construct();
      $this->load->model('test/Test_mod','tmod');
  }
 
  public function new_get()
  {
      $datas = $this->get();
      $this->data['get'] = $this->tmod->getdata($datas);
      $this->response($this->data,200);
  }


  public function new_put()
  {
      $datas = $this->put();
      $this->data['put'] = $this->tmod->putdata($datas);
      $this->response($this->data,200);
  }



}