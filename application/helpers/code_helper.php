<?php
/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 13/01/2021
 * Time: 18:11
 */

//Générer les codes pour chaque type de confidentialité
function gen_codes($droit) {

    if ($droit == 'Privée') {
        $secret = 'DHZ8FO4KWE7K';
    } elseif ($droit == 'DGI') {
        $secret = 'O5BLR0E2T7Y7';
    } else {
        $secret = 'QR5AECEP40TO';
    }

    return $secret;
}

//Générer les codes de sécurité pour les liens vers un Texte.
 function security_code($droit) {

    $secret = gen_codes($droit);

    $str1 = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZWazertyuiopqsdfghjklmwxcvbn', 100)), 100, 50);
    $str2 = substr(str_shuffle(str_repeat('123STVU4567IJKLMN890XABCDEFGHOPQRYZWazertyuiopqsdfghjklmwxcvbn', 80)), 150, 50);
    $security = $str1 . $secret . $str2;

    return $security;
}

//Extraire la code de sécurité sur les liens des textes.
 function extract_security_code($token) {

    $res = substr($token, 50, 12);

    return $res;
}

// Générer les Token opérateurs pour les Utilisateurs
 function generate_token_code() {
    $str1 = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZWazertyuiopqsdfghjklmwxcvbn', 100)), 100, 60);
    $str2 = substr(str_shuffle(str_repeat('123STVU4567IJKLMN890XABCDEFGHOPQRYZWazertyuiopqsdfghjklmwxcvbn', 80)), 150, 60);
    $security = $str1. $str2;

    return $security;
}



// Générer un token
function generate_token($n) {
    if ($n>=4){
        $str = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZWazertyuiopqsdfghjklmwxcvbn', 100)), 100, $n);
    }else {
        $str = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZWazertyuiopqsdfghjklmwxcvbn', 100)), 100, 60);
    }

    return $str;
}

//Générer les mot de passes pour les Utilisateurs
function generate_pass($length) {
    $str1 = substr(str_shuffle(str_repeat('1234567890XABCDEFGHIJKLMNOPQRSTVUYZW', 100)), 73, $length);
    return $str1;
}

function generate_code() {
    $ncu = substr(str_shuffle(str_repeat('1234567890', 20)), 40, 14);
    return $ncu;
}