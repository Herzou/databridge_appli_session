﻿// JavaScript Document
$(document).on('click','#loader-close',function(event){
	event.preventDefault();
	hide_loader();
});
function show_loader(){
    $("#Black_layer").fadeIn(350);
	$('.loader-content').html('<h4 class="text-center cell"><span class="fa fa-spinner fa-spin fa-2x Mgr-20"></span><span>En cours d\'execution</span></h4>');
}
function hide_loader(){
    $("#Black_layer").fadeOut(350);
}
function show_error(){
	$('.loader-content').html('<h4>Erreur de connection au serveur!</h4><a href="#" id="loader-close"><span>Fermer</span></a>');
}
function putloader(element){
	$(element).html('<span class="fa fa-spin fa-spinner"></span>');
}
function removeloader(element){
	$(element).html('<a id="add_im" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span>Ajouter</a>');
}

