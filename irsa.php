<?php
/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 21/01/2021
 * Time: 10:35
 */

//INITIALISATION - A parametrer et a intégrer dans les BD
$tranche = [];
$tranche [] = [600000, 0.20];
$tranche [] = [500000 , 0.15];
$tranche [] = [400000 , 0.10];
$tranche [] = [350000 ,  0.05];

//Affichage de l'exemple : Salaire = 625000
//echo(resTranche(625000, $tranche));
$salImp = 575099;

var_dump(roundX($salImp));

echo(resTranche(roundX($salImp), $tranche));


function resTranche($salaire, $tranche){
    //il faut au moins une tranche
    if(count($tranche) === 0) return 0;

    //Calculer la première tranche
    $res = max($salaire - $tranche[0][0], 0) * $tranche[0][1];

    //Calculer les tranches suivantes
    for($i=1; $i<count($tranche); $i++){
        $res +=  min(max($salaire - $tranche[$i][0],0), $tranche[$i-1][0] - $tranche[$i][0])*$tranche[$i][1];
    }
    //IRSA>2000
    return max($res,2000);

}


function roundX($x){
   return floor($x/100)*100 ;
}