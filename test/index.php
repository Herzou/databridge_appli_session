<?php
require_once 'grh/Ocidb.php';

$base = new Ocidb();
$base::init();
$lists = $base::selectAll('USERS_INFOS');

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <table class="table table-stripped">
        <tbody>
        <pre>
        <?php
        foreach ($lists as $key => $list) {
            echo '<tr>';
                echo '<td>'.$list->USERNAME.'</td>';
                echo '<td>'.$list->MATRICULE.'</td>';
                echo '<td>'.$list->NOM.'</td>';
                echo '<td>'.$list->PRENOM.'</td>';
                echo '<td>'.$list->EMAIL.'</td>';
                echo '<td>'.$list->CIN.'</td>';
            echo '</tr>';
        }
        ?>
            </pre>
        </tbody>
    </table>
</div>


</body>
</html>
