$('.c_box').click(function(){
    var test = $(this).children().find("input").prop("checked");
    var id = $(this).children().data("id");
    var element = '#'+id;
    if (test==true) {
        $(element).attr("disabled",true);
    }else{
        $(element).attr("disabled",false);
    }
    //alert(element);
});


$(document).on('keyup','.calc',function(event){
    event.preventDefault();
    var id = $(this).data("id");
    var re = $(this).val();
    var num = is_numeric(re);
    var index = parseInt($('#T_index').val(),10);
    if (num==true) {
        makesomme(id);
        maketotal(index);
    }else{
        alert('CE CHAMP N\'ACCEPTE QUE DES NOMBRES CONTENANT LES CHIFFRES 0-9 UNIQUEMENT');
        $(this).val(0);
        makesomme(id);
        maketotal(index);
    }
    //
});



//***********************************************************************************************************************************************************************************************/

function makesomme(id){
    var ppl = $('#ppl_'+id).val();
    var amd = $('#amd_'+id).val();
    var ttl = parseInt(ppl,10) + parseInt(amd,10);
    $('#ttl_'+id).val(ttl);
}

function is_numeric(num){
    var expres = /^([0-9]+)$/.test(num);
    return expres;
}

function maketotal(index){
    var Tppl = 0;
    var Tamd = 0;
    var ppl = 0;
    var amd = 0;
    for (var i = 0; i<index; i++) {
        ppl = $('#ppl_'+i).val();
        amd = $('#amd_'+i).val();
        Tppl = parseInt(Tppl) + parseInt(ppl,10);
        Tamd = parseInt(Tamd) + parseInt(amd,10);
    }
    $('#T_ppl').val(Tppl);
    $('#T_amd').val(Tamd);
    $('#T_ttl').val(Tppl+Tamd);
}


