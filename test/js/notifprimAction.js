/**
 * Created by SIFF on 01/08/2017.
 */

//Enregistrer une fiche technique
$('#createFtprim').click(function(event){
    event.preventDefault();
    show_loader();
    createNotifPrim();
});



//Envoyer le fichier annexe fiche technique
$('#AddAnnexPrim').click(function(event){
    event.preventDefault();
    var ftid = $('#ftprim_id').val();
    
    $("#Md_annexprim").modal({backdrop:"static"});
});


$(document).on('click','#send_annexprim',function(event){
    event.preventDefault();
    AddAnnexPrim();
});

// SELECTION FICHIER //
$(document).on('change','#FtPrimInputFile',function(){
        var fichier = document.getElementById('FtPrimInputFile');
        var file = fichier.files[0];
        var name = file.name;
        $('#input_annexe_prim').val(name);
});


$('.exo-edit').click(function(){
    $('#impotassuj').html('');
    $('#Md_editexo').modal({backdrop:"static"});   
    var annee = $(this).data('annee');
    var impot = $(this).data('impot');
    var nif = $('#nif').val();
    //Fonction dans le dossier action.
    //selectImpot(nif,annee,'#impassuj');     
    $('#anneeassuj').val(annee);
    alert(impot);
    $('#impotassuj').val(impot); 
});


$(document).on('change','#anneeassuj',function(){
    var annee = $('#anneeassuj').val();
    var nif = $('#nif').val();
    $('#impotassuj').html('');
    
    //Fonction dans le dossier action.
    selectImpot(nif,annee,'#impotassuj');
});


// SELECTION FICHIER //
$('#FtPrimAnnex').change(function(){
    var file = FtPrimAnnex.files[0];
    var name = file.name;
    $('#ftprimfile').html(name);
});



// SELECTION FICHIER //
$('#FtPrimNotifPdf').change(function(){
        var file = FtPrimNotifPdf.files[0];
        var name = file.name;
        $('#FtPrimSigne').html(name);
});



//Controle sur le rejet de fiche technique.
$('input[type=radio][name=status]').click(function(){
    var valid2 = $('#status3').prop('checked');
    if (valid2==true) {
        $('#motif_re').show(300);
    }else{
        $('#motif_re').hide(300);
    }
});



//Validation d'un fiche technique
$('#AddAnnexPrimSigne').click(function(event){
    event.preventDefault();
    show_loader();
    AddNotifPdf();
});


//Ajouter une date de reception � une fiche technique.
$('#addresp').click(function(event){
    event.preventDefault();
    show_loader();
    AddResponse();
});


//Inputfile annexe
// SELECTION FICHIER //
$('#FtPrimInputFile').change(function(){
        var file = FtPrimInputFile.files[0];
        var name = file.name;
        $('#FtPrimInputFileName').html(name);
});

/*************************************************************************************************************************************************************************************************************************************/
//Enregistrer une fiche technique
function createNotifPrim() {
    $('#md_msg_err').html('');
    $.ajax({
        url: base_url+"notifprim/createftprim",
        method: "POST",
        dataType:"json",
        data: $('#all_exo').serialize(),
        success: function (response) {
            if (response.statut==true) {
                location.reload(true);
            }else{
                hide_loader();
                $('#Md_error').modal({backdrop:"static"});
                $('#md_msg_err').append('<span>'+response.errors.error+'</span>');
            }
        },
        error: function () {
            show_error();
        }
    });
}


function AddAnnexPrim(){
    $.ajax({
        url:base_url+"notifprim/addannexeprim",
        method:"POST",
        dataType:"json",
        contentType:false,
        data: new FormData(document.getElementById('annexprim_form')),
        processData:false,
        success: function(data){
            console.log(data);
            if (data.statut==false) {
                $("#form_error").html(data.error);
            }else{
                location.reload(true);
            }
        },
        error: function(){
            show_error();
        }
    });
}


function AddNotifPdf(){
    $.ajax({
        url:'',
        method:"POST",
        dataType:"json",
        contentType:false,
        data: new FormData(document.getElementById('all_exo')),
        processData:false,
        success: function(data){
            //console.log(data);
            if( data.status===true ){
                location.reload(true);
            }else{
                hide_loader();
                $('#classic_msg').html('<span>'+data.msg+'</span>');
                $('#classic_error').modal({backdrop:"static"});
            }
        },
        error: function(){
            show_error();
        }
    });
}


function AddResponse(){
    $.ajax({
        url:'core/reception.php',
        method:"POST",
        dataType:"json",
        data:$('#RespForm').serialize(),
        success:function(data){
            console.log(data);
            location.reload(true);
        }
    });
}


