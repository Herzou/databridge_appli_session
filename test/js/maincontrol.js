$(document).ready(function(){
    
	init_datepicker();
	set_date_picker('#d_date');
    set_date_picker('#recep_date');
    set_date_picker('.date-lettre');
    set_date_picker('#rds_recep');
    set_date_picker('.comfisc_date');
    set_date_picker('.pcs-date');
	
	//Initialiser les popovers liste
	$('.data-row').popover({
		title:'<span class="Bold">Impôts & Exercices</span>',
		content:function(){
			var fid = $(this).attr("data-id"); 
			var fncu = $(this).attr("data-ncu");
			
			//recuperer les details	
			 $.ajax({
					  url:"core/get_ft.php",
					  method:"POST",
					  //dataType:"json",
				 	  async: false,
					  data:{ft_id:fid,ncu:fncu},
					  success: function(response){
						  $('#info').html(response);
					  },
				 	  error: function(){
						  $('#info').html('<span class="Red"> Erreur!</span>');
					  }
			 });
			
			return $('#info').html();
		},
		html:true,
		trigger:'hover',
		placement:'right',
		container:'body'
	});

    //Ajouter une verification à un dossier.
	$('#add_verif').click(function(){
		//var nif = $('#nif').val();
		//var nombre = $('#verif_count').val();
        var ncu = $('#ncu14').val();
		//var ddate = $('#d_date').val();
		var exo = $('#exo').val();
		var imp = $('#imp').val();

            $.ajax({
                url:'core/add_control.php',
                method:'POST',
                dataType:"json",
                data:{action:'check_exo',ncu:ncu,exo:exo,imp:imp},
                success: function(data){
                    //console.log(data);
                    if (data.status===true) {
                        alert('CE NIF EST DEJA CONTROLE SUR CETTE ANNEE D\'EXERCICE AVEC CE TYPE D\'IMPOT');
                    }else{
                        //alert('ajout');
                        add_exo_control();
                    }
                }
            }); 

		/*var data = '<div class="row"><div class="col-md-3 col-md-offset-1 text-center">'
                    +nif+'</div><div class="col-md-3 text-center">'+exo+'</div><div class="col-md-2 text-center">'
                    +imp+'</div><div class="col-md-3 text-center">'+ddate+'</div></div><hr>';  

		$('#list_control').prepend(data);*/
	});


    //Rechercher et afficher les details d'un NIF trouvé.
    $('#search_nif').click(function(){
        $('#cre_doss').show(400);
        var nif = $('#nif').val();
		show_loader();

        $.ajax({
            url:'core/checkdata.php',
            method:'POST',
            dataType:"json",
            data:{action:'check_control',nif:nif},
            success:function(data){
                //console.log(data);
				hide_loader();
                if (data.status==true) {
                    $('#modal_1').modal({backdrop:"static"});
                    $('.row #message').html('<span>'+data.msg+'</span>');
                }else{
                    show_nif_infos(data);
                }  
            },
			error:function(){
				show_error();
			}
        });
    });
    $('#oui').click(function(){
            get_nif_infos();
            $('#modal_1').modal('hide');
    });



    //Créer un dossier avec NCU
    $('#cre_doss').click(function(event){
        event.preventDefault();
		show_loader();
        var nif = $('#nif').val();
        var rsoc = $('#rsoc').val();
        var act = $('#act').val();
        var adress = $('#adress').val();
        var ctype = $('input[type=radio][name=type]:checked').attr('value');
        var cdbur = $('#cdbur').val();
		var cfgest = $('#gest').val();

        if (nif!='' && nif>999999999) {
            $.ajax({
                url:'core/nouv_doss.php',
                method:'POST',
                dataType:"json",
                data:{ncu:'create',nif:nif,rsoc:rsoc,adress:adress,act:act,cdbur:cdbur,type:ctype,cfgest:cfgest},
                success: function(data){
                    console.log(data);
					$('#page_title').slideUp(300);
					$('.assuj').show(400);
					$('#ncu_area').slideDown(300);
					var htmldata ='<div class="row Mgt-15"><div class="col-md-6 text-center"><h2 style="font-family:Roboto,Verdana;">NCU - '+data.ncu+
						'</h2><input id="ncu14" type="text" name="ncu" value="'+data.ncu+'" style="display:none;"></div><div class="col-md-6 text-center"><img src="files/ncu_files/'+data.barcode+'" alt="NCU barcode"></div></div>';
					$('#ncu_area').html(htmldata);
					$('#cre_doss').hide(200);
					$('#conf_doss').show(300);
					$('#list_verif').show(400);
					hide_loader();
                    //show_ncu(data);
                },
				error:function(){
					show_error();
				}
            });

        }

    });

    //Ajouter un vérificateur dans la liste affichée
    //Vérifie si un verificateur est déja sur le dossier courant.
    $(document).on('click','#im-spinner',function(event){
        event.preventDefault();
		putloader('#im-spinner');
        var matricule = $('#im').val();
		var im = matricule.replace(/ /g,'');
		var chars = im.length;
        var ncu = $('#ncu14').val();
        var verif = $('#verif').val();
        var nif = $('#nif').val();
        //alert(ncu+'  '+im);
        if((im!=='') && (im>111111) && (chars===6)){
			//alert(im);
            $.ajax({
                url:'core/checkdata.php',
                method:'POST',
                dataType:"json",
                data:{action:'check_verif',nif:nif,ncu:ncu,verif:verif,im:im},
                success: function(data){
					removeloader('#im-spinner');
                       //console.log(data);
                    if (data.status===true) {
                        $('#modal_2').modal({backdrop:"static"});
                        $('.row #message').html('<span>'+data.msg+'</span>');
                    }else{
                        //alert('false');
                        add_im_data();
                    }                    
                }                
            });
        }else{
			alert('NUMERO MATRICULE INVALIDE');
		}

    });


    //Etape_1 Ajouter une ligne sur le fiche technique
    $(document).on('click','#add_fiche',function(){
        $('#modal_add_ft').modal({backdrop:"static"}); 
        var ft_id = $('#ft_id').val();
        var ncu = $('#dos_ncu').val();
        // transfert de valeurs
        $('#md_ncu').val(ncu);
        $('#md_ft_id').val(ft_id);         
    });

    //Confirmer le dossier
    $('#conf_doss').click(function(){
		var nbr_verif = $('#nombre_verif').val();
		var nombre = $('#verif_count').val();
		
		if(nbr_verif<=0){
			
			$('#modal_verif_msg').modal({backdrop:"static"});	
			
		}else{
			if(nombre <= 0){
				$('#modal_exo_msg').modal({backdrop:"static"});
			}else{

				$('#modal_dossier').modal({backdrop:"static"});

			}			
		}
    });


    //Etape_2 Ajouter une ligne sur le fiche technique
    $('#add_ft_exo').click(function(event){
        event.preventDefault();
		$('#modal_add_ft').modal('hide');
		show_loader();
        //var ft_id = $('#ft_id').val();
        //var ncu = $('#dos_ncu').val();
            //alert(ft_id +' - '+ ncu);
            $.ajax({
                url:"core/add_dft.php",
                method:"POST",
                dataType:"json",
                data:$('#mdl_ft').serialize(),
                success: function(data){
					hide_loader();
                    //console.log(data);
                    if (data.status==true) {
                        alert(data.msg);
                    }else{
                        location.reload(true);
                    }
                },
				error:function(){
					show_error();
				}
            });
    });


    //Controle sur le rejet de Visa.
    $('input[type=radio][name=valid]').click(function(){
        var valid2 = $('#valid2').prop('checked');
        if (valid2==true) {
            $('#motif_rejet').show(300);
        }else{
            $('#motif_rejet').hide(300);  
        }
    });

    //Controle sur le rejet de fiche technique.
    $('input[type=radio][name=status]').click(function(){
        var valid2 = $('#status2').prop('checked');
        if (valid2==true) {
            $('#motif_re').show(300);
        }else{
            $('#motif_re').hide(300);  
        }
    });

    //Valider une demande d'autorisation de verificaation.
    $('#btn_visa_valid').click(function(){
		
		show_loader();
		
        //var valid2 = $('#valid2').prop('checked');
        var ncu = $('#ncu14').val();
        var statut = $('input[type=radio][name=valid]:checked').val();
        var date_visa = $('#dos_date_visa').val();
		var motif = 'none';
		var rsoc = $('#dos_rsoc').html();
		var nif = $('#dos_nif').html();
		var addr = $('#dos_addr').html();
		var cf_gest = $('#cf_gest').val();
		
		
		//alert(rsoc);

        if (statut==='rejected') {
             motif = $('#rej_motif').val(); 
        }else{
             motif = 'none';
        }

        $.ajax({
            url:'core/validation.php',
            method:'POST',
            dataType:"json",
            data:{action:'validation',ncu:ncu,status:statut,date_visa:date_visa,rej_motif:motif,rsoc:rsoc,nif:nif,address:addr,cf:cf_gest},
            success:function(data){
               //console.log(data);
                location.reload(true);
            },
			error: function(){
				show_error();
			}
        });
    });


    //Creation d'une fiche technique
    $('#nouv_fiche').click(function(){
        $(this).hide(200);
        $('#dft_form').slideDown(300);

        var ncu = $('#ncu14').val();        
        //var valtype = $('input[type=checkbox][name=notif]').prop('checked');
        var cf_gest = $('#cf_gest').val();
        var cf_cdbur = $('#cf_cdbur').val();
        var ft_file = 'none';
        var ft_type = 'primitive';


        $.ajax({
            url:"core/create_ft.php",
            method:"POST",
            dataType:"json",
            data:{action:'fichetech',ncu:ncu,cf_gest:cf_gest,cd_bur:cf_cdbur,ft_fichier:ft_file,ft_type:ft_type},
            success:function(data){
                console.log(data);
                //$('#fiche_tec').html(data.exolist);
                $('#ft_id').val(data.ft_id);
                $('#dos_id').val(data.ft_id);
                //window.location.href = 'index.php';
            }
        });
    });

    //Enregistrer une fiche technique
    $('#ft_save').click(function(event){
        event.preventDefault();
		show_loader();
        $.ajax({
            url:"core/save_ft.php",
            method:"POST",
            data: $('#all_exo').serialize(),
            success:function(data){
                console.log(data);
                //$('#var').html(data);
                location.reload(true);
            },
			error:function(){
				show_error();
			}
        });

    });


    //Upload annexe_primitive

    //selection fichier
    $('#annexe_file').change(function(){

        var file = annexe_file.files[0];
        var name = file.name;
        $('#annexe_label').html(name);
    });
    $('#annexe_file1').change(function(){

        var file = annexe_file1.files[0];
        var name = file.name;
        $('#annexe_label1').html(name);
    });
    $('#ntpr_file').change(function(){
        //alert('ok');
        var file = ntpr_file.files[0];
        var name = file.name;
        $('#ntpr_label').html(name);
    });
    $('#lrfile').change(function(){
        //alert('ok');
        var file = lrfile.files[0];
        var name = file.name;
        $('#filename').html(name);
    }); 
    $('#rds_file').change(function(){
        //alert('ok');
        var file = rds_file.files[0];
        var name = file.name;
        $('#rds_filename').html(name);
    });
    $('#def_file').change(function(){
        //alert('ok');
        var file = def_file.files[0];
        var name = file.name;
        $('#def_filename').html(name);
    });
    $('#rds_annexe1').change(function(){
        //alert('ok');
        var file = rds_annexe1.files[0];
        var name = file.name;
        $('#rdsfilename').html(name);
    });
    $('#def_annexe1').change(function(){
        //alert('ok');
        var file = def_annexe1.files[0];
        var name = file.name;
        $('#deffilename').html(name);
        $('.def_info').html('<span class="Italic Red">(Cliquer sur le button enregistrer pour valider l\'action.)</span>');		
    });
    $('#rec_file').change(function(){
        //alert('ok');
        var file = rec_file.files[0];
        var name = file.name;
        $('#rec_filename').html(name);
        $('#recep_file').val(name);
    });
    $('#lettre_saisine').change(function(){
        //alert('ok');
        var file = lettre_saisine.files[0];
        var name = file.name;
        $('#filesaisine').html(name);
        $('input[name=file_saisine]').val(name);
    });
    $('#lettre_observ').change(function(){
        //alert('ok');
        var file = lettre_observ.files[0];
        var name = file.name;
        $('#fileobserv').html(name);
        $('input[name=file_observ]').val(name);
    });
    $('#lettre_avis').change(function(){
        //alert('ok');
        var file = lettre_avis.files[0];
        var name = file.name;
        $('#fileavis').html(name);
        $('input[name=file_avis]').val(name);
    });
    $('#rcl_file').change(function(){
        //alert('ok');
        var file = rcl_file.files[0];
        var name = file.name;
        $('#rcl_filename').html(name);
    });    
	$('#rds_file3').change(function(){
        //alert('ok');
        var file = rds_file3.files[0];
        var name = file.name;
        $('#rds_annexe3').html(name);
    });	
	$('#def_valid_file').change(function(){
        //alert('ok');
        var file = def_valid_file.files[0];
        var name = file.name;
        $('#def_valid_filename').html(name);
    });




    //Envoyer le fichier annexe fiche technique
    $('#annexe_prim').click(function(event){
        event.preventDefault();
		show_loader();
        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
			dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('all_exo')),
            processData:false,
            success: function(data){
                //console.log(data);
				if( data.status===true ){
                	location.reload(true);			
				}else{
					hide_loader();
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});
				}					
            },
			error: function(){
				show_error();
			}
        });

    });

    //Envoyer le fichier annexe fiche technique corrigée
    $('#annexe_prim1').click(function(event){
        event.preventDefault();
		show_loader();
        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('valid_annexe')),
            processData:false,
            success: function(data){
                //console.log(data);
				if( data.status===true ){
					hide_loader();
					$('#ft_annex_btn').html('<label id="annexe_label" for="" class="control-label"><a href="files/doc_files/'+data.file+'" target="_blank">'+data.file+'</a></label><div class="Italic Red"> (Cliquer sur le button enregistrer pour valider l\'action.)</div>');
					//location.reload(true);
				 }else{
					hide_loader();
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});					 
				 }
            },
			error: function(){
				show_error();
			}
        });

    });
	
	//Envoyer les notification primitive Validée et corrigée
	$('#ntpr_prim').click(function(){
		show_loader();
		
        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
			dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('all_exo')),
            processData:false,
            success: function(data){
				if(data.status===true){
					hide_loader();
					location.reload(true);
				 }else{
					 hide_loader();
					 $('#classic_msg').html('<span>'+data.msg+'</span>');
					 $('#classic_error').modal({backdrop:"static"});
				 }					
            },
			error: function(){
				show_error();
			}
        });		
	});

    //Envoyer le fichier annexe fiche technique de redressement
    $('#rds_file_send').click(function(event){
        event.preventDefault();
		show_loader();

        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
			dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('rds_data_form')),
            processData:false,
            success: function(data){
                //console.log(data);
				hide_loader();
				if(data.status===true){
                	location.reload(true);
				}else{
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});
				}
            },
			error: function(){
				show_error();
			}
        });

    });

    //ENVOE NOTIF RDS SIGNEE TAMPONNEE
    $('#notif_def_send').click(function(event){
        event.preventDefault();
		show_loader();

        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
			dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('rds_data_form')),
            processData:false,
            success: function(data){
                //console.log(data);
				hide_loader();
				if(data.status===true){
                	location.reload(true);
				}else{
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});
				}
            },
			error:function(){
				show_error();
			}
        });

    });

    //Envoyer le fichier annexe fiche technique definitive
    $('#def_file_send').click(function(event){
        event.preventDefault();
		show_loader();
        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
			dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('def_data_form')),
            processData:false,
            success: function(data){
				hide_loader();
				if(data.status===true){
                	location.reload(true);
				}else{
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});
				}
            },
			error:function(){
				show_error();
			}
        });

    });

    //Envoyer le fichier annexe fiche technique redressement corrigée
    $('#rds_validation').click(function(event){
        event.preventDefault();
		show_loader();
        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('valid_rds_annexe')),
            processData:false,
            success: function(data){
				hide_loader();
                if(data.status===true){
                	location.reload(true);
				 }else{
					 $('#classic_msg').html('<span>'+data.msg+'</span>');
					 $('#classic_error').modal({backdrop:"static"});
				 }					
            },
			error: function(){
				show_error();
			}
        });

    });

    //Envoyer le fichier annexe fiche technique définitive corrigée
    $('#def_validation').click(function(event){
        event.preventDefault();
		show_loader();
        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('valid_def_annexe')),
            processData:false,
            success: function(data){
				hide_loader();
                if(data.status===true){
                	location.reload(true);
				 }else{
					 $('#classic_msg').html('<span>'+data.msg+'</span>');
					 $('#classic_error').modal({backdrop:"static"});
				 }					
            },
			error: function(){
				show_error();
			}
        });
    });
	
	//ENVOE NOTIF DEFINITIVE SIGNEE TAMPONNEE
    $('#signed_def_file').click(function(event){
        event.preventDefault();
		show_loader();

        $.ajax({
            url:'core/upload_file.php',
            type:"POST",
			dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('def_data_form')),
            processData:false,
            success: function(data){
                //console.log(data);
				hide_loader();
				if(data.status===true){
                	location.reload(true);
				}else{
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});
				}
            },
			error:function(){
				show_error();
			}
        });

    });

    //Validation d'un fiche technique
    $('#ft_validation').click(function(){
		show_loader();
        var ncu = $('#ncu14').val();
        var statut = $('input[type=radio][name=status]:checked').val();
        var ft_id = $('#dos_id').val();
        var date_valid = $('#today').val();
		var motif='none';

        if (statut==='rejected') {
            motif = $('#motif_rz').val(); 
        }else{
            motif = 'none';
        }

        $.ajax({
            url:'core/validate_ft.php',
            method:'POST',
            dataType:"json",
            data:{action:'ft_valid',ncu:ncu, ft_id:ft_id, status:statut, date_valid:date_valid, motif:motif},
            success:function(data){
               //console.log(data);
               location.reload(true);
            },
			error: function(){
				show_error();
			}
        });
       
    });


    //Ajouter une date de reception à une fiche technique.
    $('#addresp').click(function(){
               
            var ncu = $('#dos_ncu').val();
            var ft_id = $('#ft_id').val();
            var rdate = $('#recep_date').val();
            var ft_type = 'primitive';
            //alert(ncu +'-'+ ft_id+'-'+rdate);

        $.ajax({
            url:'core/reception.php',
            method:"POST",
            dataType:"json",
            data:{action:'reception',ncu:ncu, ft_id:ft_id,ft_rdate:rdate,ft_type:ft_type},
            success:function(data){
               console.log(data);
               location.reload(true);
            }
        });

    });

    //Ajouter une date de reception à une fiche technique de redressement.
    $('#rds_addresp').click(function(){
            //alert('ok');
        $.ajax({
            url:'core/reception.php',
            method:"POST",
            dataType:"json",
            data: $('#rds_response_form').serialize(),
            success:function(data){
               console.log(data);
               location.reload(true);
            }
        });

    });

    //Ajouter une date de reception à une fiche technique definitive.
    $('#def_addresp').click(function(){
            //alert('ok');
        $.ajax({
            url:'core/reception.php',
            method:"POST",
            dataType:"json",
            data: $('#def_response_form').serialize(),
            success:function(data){
               console.log(data);
               location.reload(true);
            }
        });

    });
	
    //formulaire Lettre de reponse.
    $('input[type=radio][name=lr_valid]').click(function(){
        var option = $('input[type=radio][name=lr_valid]:checked').val();
        //alert(option);
        if (option==='yes' || option==='pending') {
           $('#lr_form').show(300); 
        }else{
            //$('input[type=text]').val('');
            //$('textarea').val('');
            $('#lr_form').hide(300); 
        }
    });

    //Enregistrer une lettre de reponse
    $('#savelr').click(function(event){
        event.preventDefault();
		show_loader();

        $.ajax({
            url:"core/add_lr.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('lettre_rep')),
            processData:false,
            success: function(data){
                hide_loader();
				if(data.status===true){
                	location.reload(true);
				 }else{ 
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});
				 }					
            },
			error: function(){
				show_error();
			}
        });

    });

    //Enregistrer une lettre de reponse
    $('#savercl').click(function(event){
        event.preventDefault();

        $.ajax({
            url:"core/save_rcl.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('rcl_rep')),
            processData:false,
            success: function(data){
                console.log(data);
                location.reload(true);
            }            
        });

    });
    //Mettre à jour un Reclamation
    $('#updatercl').click(function(event){
        event.preventDefault();

        $.ajax({
            url:"core/save_rcl.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('rcl_rep')),
            processData:false,
            success: function(data){
                console.log(data);
                location.reload(true);
            }            
        });

    });

    //Mettre à jour une lettre de reponse
    $('#updatelr').click(function(event){
        event.preventDefault();
		show_loader();
        $.ajax({
            url:"core/add_lr.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('lettre_rep')),
            processData:false,
            success: function(data){
				if(data.status===true){
                	location.reload(true);
				 }else{
					 hide_loader();
					$('#classic_msg').html('<span>'+data.msg+'</span>');
					$('#classic_error').modal({backdrop:"static"});
				 }	
            },
			error: function(){
				show_error();
			}
        });

    });

    //Ajouter une fiche technique de redressement.
    $('#save_rds').click(function(){
        show_loader();
        $.ajax({
            url:"core/create_ft_rds.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('ft_rds_input')),
            processData:false,
            success: function(data){
                //console.log(data);
                location.reload(true);
            },
			error: function(){
				show_error();
			}

        });
    });

    //Ajouter une fiche technique definitive.
    $('#save_def').click(function(){
        //alert('ok');
		show_loader();
        $.ajax({
            url:"core/create_ft_def.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('ft_def_input')),
            processData:false,
            success: function(data){
				
                console.log(data);
                location.reload(true);
            },
			error:function(){
				show_error();
			}

        });
    });

    //formulaire commission fiscale.
    $('input[type=radio][name=com_fisc]').click(function(){
        var option = $('input[type=radio][name=com_fisc]:checked').val();
        //alert(option);
        if (option==='yes') {
           $('#com_fisc').show(300); 
        }else{
            $('input[type=text]').val('');
            //$('textarea').val('');
            $('#com_fisc').hide(300); 
        }
    });

    //Enregistrer une commission fiscale.
    $('#save_comfisc').click(function(){
        $.ajax({
            url:"core/com_fisc.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('comfisc_form')),
            processData:false,
            success: function(data){
                console.log(data);
                location.reload(true);
            } 

        });

    });


    //mettre à jour une commission fiscale.
    $('#update_comfisc').click(function(){
        $.ajax({
            url:"core/com_fisc.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('comfisc_form')),
            processData:false,
            success: function(data){
                console.log(data);
                location.reload(true);
            } 

        });

    });

    
    //Enregistrer un recouvrement.
    $('#savercv').click(function(){


        $.ajax({
            url:"core/save_rcv.php",
            method:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('recouv_input')),
            processData:false,
            success: function(data){
                console.log(data);
                location.reload(true);
            } 

        });

    });

    //formulaire recouvrement
    $('input[type=radio][name=recouvre]').click(function(){
        var option = $('input[type=radio][name=recouvre]:checked').val();
        //alert(option);
        if (option==='yes') {
           $('#recouv_bloc').show(300); 
        }else{
            //$recouv_bloc('textarea').val('');
            $('#recouv_bloc').hide(300); 
        }
    });


    //Calcule des sommes recouvrement
    $('#rc_ppl').keyup(function(){
         var ppl = $('#rc_ppl').val();
         var amd = $('#rc_amd').val();
         var summe  = Math.round(ppl) + Math.round(amd);
         $('#rc_total').val(summe); 
    });
    $('#rc_amd').keyup(function(){
         var ppl = $('#rc_ppl').val();
         var amd = $('#rc_amd').val();
         var summe  = Math.round(ppl)+Math.round(amd);
         $('#rc_total').val(summe); 
    });

    //************************************************* POPOVER EDITEUR *************************************************************/
	
    $(document).on('click','.edit-infra',function(){
		var id =$(this).attr("data-id");
		//alert(id);
		$('#dft_id').val(id);
		
		$('#modal_infra').modal({backdrop:"static"});
    });
	
    $(document).on('click','.edit-mth',function(){
		var id =$(this).attr("data-id");
		//alert(id);
		$('#meth_id').val(id);		
		
		$('#modal_method').modal({backdrop:"static"});	
    });
	
    $(document).on('click','.edit-doc',function(){
		var id =$(this).attr("data-id");
		//alert(id);
		$('#doc_id').val(id);	
		
		$('#modal_doc').modal({backdrop:"static"});	
    });
	
    $(document).on('click','.edit-exer',function(event){
		event.preventDefault();
		show_loader();
		var id =$(this).attr("data-id");
		var ncu = $('#ncu14').val();
		//alert(id);
		//ajax description here!
 		$('#exo_id').val(id);
			
		 $.ajax({
				  url:"core/edit_dft.php",
				  method:"POST",
				  dataType:"json",
				  data:{action:'load_exo',ncu:ncu,dft_id:id},
				  success:function(response){
					 console.log(response);
					  $('#modif_exo').html(response.exos);
					  $('#modif_imp').html(response.impot);
					  $('#modif_ppl').val(response.ppl);
					  $('#modif_amd').val(response.amd);
					  $('#modif_id').val(response.ft_id);
					  hide_loader();
					  $('#modal_edexo').modal({backdrop:"static"});
				  },
			 	  error:function(){
					  show_error();
				  }
		 });
		
	});     
	
	//AFFICHER EDITION MONTANT REDRESSEMENT
	$(document).on('click','.rds-edit-exer',function(event){
		event.preventDefault();
		show_loader();
		var id =$(this).attr("data-id");
		var ncu = $('#ncu14').val();
		//alert(id);
		//ajax description here!
 		$('#rdsexo_id').val(id);
		$('#ft_type').val('redressement');
		$('#update_mnt').addClass('save-mnt-rds');		
		 $.ajax({
				  url:"core/edit_dft.php",
				  method:"POST",
				  dataType:"json",
				  data:{action:'load_exo',ncu:ncu,dft_id:id},
				  success:function(response){
					 console.log(response);
					  $('#rds_modif_ppl').val(response.ppl);
					  $('#rds_modif_amd').val(response.amd);
					  $('#rds_modif_id').val(response.ft_id);
					  hide_loader();
					  $('#modal_montant').modal({backdrop:"static"});
				  },
			 	  error:function(){
					  show_error();
				  }
		 });
		
	}); 
		
	//AFFICHER EDITION MONTANT FIHE TECH DEFINITIVE
	$(document).on('click','.def-edit-exer',function(event){
		event.preventDefault();
		show_loader();
		var id =$(this).attr("data-id");
		var ncu = $('#ncu14').val();
		//alert(id);
		//ajax description here!
 		$('#rdsexo_id').val(id);
		$('#ft_type').val('definitive');
		$('#update_mnt').addClass('save-mnt-def');	
		 $.ajax({
				  url:"core/edit_dft.php",
				  method:"POST",
				  dataType:"json",
				  data:{action:'load_exo',ncu:ncu,dft_id:id},
				  success:function(response){
					 console.log(response);
					  $('#rds_modif_ppl').val(response.ppl);
					  $('#rds_modif_amd').val(response.amd);
					  $('#rds_modif_id').val(response.ft_id);
					  hide_loader();
					  $('#modal_montant').modal({backdrop:"static"});
				  },
			 	  error:function(){
					  show_error();
				  }
		 });
		
	}); 
	
	//AFFICHER EDITER OBSERVATION FICHE TECHNIQUE PRIMITIVE.
	$(document).on('click','.edit-obs',function(event){
		event.preventDefault();
		var id =$(this).attr("data-id");
		//alert(id);
		$('#obs_id').val(id);
		$('#fiche_type').val('primitive');
		$('#save_obs').addClass('save-primitive');
		$('#modal_obs').modal({backdrop:"static"});	
	});	
	
	//AFFICHER EDITER RECOUPEMENTS
	$(document).on('click','.edit-recoup',function(event){
		event.preventDefault();
		var id =$(this).attr("data-id");
		//alert(id);
		$('#recoup_id').val(id);
		$('#modal_recoup').modal({backdrop:"static"});	
	});
	 
	//AFFICHER EDITION OBSERVATION FICHE TECHNIQUE DE REDRESSEMENT
	$(document).on('click','.rds-edit-obs',function(event){
		event.preventDefault();
		var id =$(this).attr("data-id");
		//alert(id);
		$('#obs_id').val(id);
		$('#save_obs').addClass('rds-save-obs');
		$('#fiche_type').val('redressement');
		$('#modal_obs').modal({backdrop:"static"});			
	});
		 
	//AFFICHER EDITION OBSERVATION FICHE TECHNIQUE DE DEFINITIVE
	$(document).on('click','.def-edit-obs',function(event){
		event.preventDefault();
		var id =$(this).attr("data-id");
		//alert(id);
		$('#obs_id').val(id);
		$('#save_obs').addClass('def-save-obs');
		$('#fiche_type').val('definitive');
		$('#modal_obs').modal({backdrop:"static"});			
	});
	
	
	
	//*************************************** END POPOVER EDITEUR **************************************************************/
	
	//Delete exo control
	$(document).on('click','.delete-exo',function(){
		var id = $(this).attr('data-id');
		var ncu = $('#ncu14').val();
		//alert(id);
		$.ajax({
			url:"core/delete_data.php",
			method:"POST",
			dataType:"json",
			data:{action:'exo',ncu:ncu,id:id},
			success: function(data){
				//console.log(data);
				$('#list_control').html(data.list);
				$('#verif_count').val(data.nombre);
			}
		});
	});
	
	//Delete Verificateur
	$(document).on('click','.delete-verif',function(){
		var id = $(this).attr('data-id');
		var ncu = $('#ncu14').val();
		//alert(id);
		$.ajax({
			url:"core/delete_data.php",
			method:"POST",
			dataType:"json",
			data:{action:'verif',ncu:ncu,id:id},
			success: function(data){
				console.log(data);
				$('#vrlist').html(data.list);
				$('#nombre_verif').val(data.nombre);
			}
		});
	});
	
	//EDITER NATURE D'INFRACTION
	$(document).on('click','#save_infra',function(){
		$('#modal_infra').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_infra').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#fiche_tec').html(data.liste);
				$('#edit_infra input[type=text]').val('');
				//$('#edit_infra input[type=checkbox]').unchecked();
            },
			error:function(){
				$('#modal_infra').modal('hide');
				show_error();
			}

        });
	});
	
	//EDITER METHODE
	$(document).on('click','#save_meth',function(){
		$('#modal_method').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_method').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#fiche_tec').html(data.liste);
				$('#edit_method input[type=text]').val('');
				//$('#edit_infra input[type=checkbox]').unchecked();
            },
			error:function(){
				$('#modal_method').modal('hide');
				show_error();
			}

        });
	});
	
	//EDITER DOCUMENT CONSULTES
	$(document).on('click','#save_doc',function(){
		$('#modal_doc').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_docs').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#fiche_tec').html(data.liste);
				$('#edit_docs input[type=text]').val('');
				//$('#edit_infra input[type=checkbox]').unchecked();
            },
			error:function(){
				$('#modal_doc').modal('hide');
				show_error();
			}

        });
	});
	
	
	//EDITER EXO & MONTANT
	$(document).on('click','#save_exo',function(event){
		event.preventDefault();
		$('#modal_edexo').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_exo').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#fiche_tec').html(data.liste);
				$('#edit_method input[type=text]').val('');
            },
			error:function(){
				$('#modal_edexo').modal('hide');
				show_error();
			}

        });
	});	
	
		
	//SAVE MONTANT MODIFIE REDRESSEMENT
	$(document).on('click','.save-mnt-rds',function(event){
		event.preventDefault();
		//$('#modal_edexo').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_montant').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#modal_montant').modal('hide');
				$('#data-row').html(data.liste);
            },
			error:function(){
				$('#modal_edexo').modal('hide');
				show_error();
			}

        });
	});	
			
	//SAVE MONTANT MODIFIE DEFINITIF
	$(document).on('click','.save-mnt-def',function(event){
		event.preventDefault();
		//$('#modal_edexo').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_montant').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#modal_montant').modal('hide');
				$('#def-data-row').html(data.liste);
            },
			error:function(){
				$('#modal_edexo').modal('hide');
				show_error();
			}

        });
	});	
	
	
	//EDITER NOTIF PRIMITIVE OBSERVATION
	$(document).on('click','.save-primitive',function(event){
		event.preventDefault();
		$('#modal_obs').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_observ').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#ft_observ').html(data.liste);
				$('#edit_observ textarea').hml('');
            },
			error:function(){
				$('#modal_obs').modal('hide');
				show_error();
			}

        });
	});
	
	
	//SAVE REDRESSEMENT OBSERVATION
	$(document).on('click','.rds-save-obs',function(event){
		event.preventDefault();
		$('#modal_obs').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_observ').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#rds-ft-observ').html(data.liste);
				$('#edit_observ textarea').hml('');
            },
			error:function(){
				$('#modal_obs').modal('hide');
				show_error();
			}

        });		
	});
		
	
	//SAVE DEFINITIVE OBSERVATION
	$(document).on('click','.def-save-obs',function(event){
		event.preventDefault();
		$('#modal_obs').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_observ').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#def-ft-observ').html(data.liste);
				$('#edit_observ textarea').hml('');
            },
			error:function(){
				$('#modal_obs').modal('hide');
				show_error();
			}

        });		
	});
	
	//EDITER RECOUPEMENTS UTILISES
	$(document).on('click','#save_recoup',function(event){
		event.preventDefault();
		$('#modal_recoup').modal('hide');
		show_loader();
		
        $.ajax({
            url:"core/edit_dft.php",
            method:"POST",
            dataType:"json",
            data:$('#edit_recoups').serialize(),
            success: function(data){
                //console.log(data);
                hide_loader();
				$('#recoup_liste').html(data.liste);
            },
			error:function(){
				$('#modal_recoup').modal('hide');
				show_error();
			}

        });
	});
	
	
	
	//AJOUTER UNE PIECE jOINTE AU DOSSIER
	$('#add_pcs').click(function(event){
		event.preventDefault();
		show_loader();
		
       $.ajax({
            url:"core/add_pcs.php",
            type:"POST",
            dataType:"json",
            contentType:false,
            data: new FormData(document.getElementById('pcs')),
            processData:false,
            success: function(data){
                //console.log(data);
				hide_loader();
				if(data.statut===true){
				 	$('#pcs_liste').html(data.list);
				 }else{
					 alert(data.msg); 
				 }	
            },
			error: function(){
				show_error();
			}		   
        });  
	});
	
	
	$('#file').change(function(){
		$('#ft_annex_btn').html('<label id="annexe_label" for="" class="control-label"><a href="files/doc_files/'+data.file+'" target="_blank">'+data.file+'</a></label><div class="Italic Red"> (Cliquer sur le button enregistrer pour valider l\'action.)</div>');
	});
		
														 /*=============================*/
                                                        /*       FONCTIONS  UTILES     */
                                                       /*=============================*/

        //initialiser le datepicker en langue français
        function init_datepicker(){
            $.datepicker.regional['fr'] = {
                closeText: 'Fermer',
                prevText: '<Préc',
                nextText: 'Suiv>',
                currentText: 'Courant',
                monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
                    'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
                monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
                    'Jul','Aoû','Sep','Oct','Nov','Déc'],
                dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
                dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
                dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
                weekHeader: 'Sm',
                //dateFormat: 'dd/mm/yy',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
            $.datepicker.setDefaults($.datepicker.regional['fr']);
        }

        function set_date_picker(element){
            $(element).datepicker({
                dateFormat: 'dd/mm/yy',
                altFormat: "dd/mm/yy"
            });
            /*$(".fdate").datepicker({
                 dateFormat: 'dd/mm/yy',
                altFormat: "dd/mm/yy"
            });*/
        }

        function show_nif_infos(data){
                $('#rsoc').val(data.RSOC);
                $('#act').val(data.ACT);
                $('#gest').val(data.CENTRE);
                $('#adress').val(data.ADRESSE);
                $('#verif').val(data.CENTRE);
                $('#d_date').val(data.date);
                $('#cdbur').val(data.BUREAU);
        }

        function get_nif_infos(){
            var nif = $('#nif').val();
            $.ajax({
                url:'core/recherchenif.php',
                method:'POST',
                dataType:"json",
                data:{nif:nif},
                success: function(data){
                    //console.log(data);
                    show_nif_infos(data);
                }
            });            
        }

        function add_im_data(){
            var im = $('#im').val().replace(/ /g,'');
            var ncu = $('#ncu14').val();
            var verif = $('#verif').val();
            //var nif = $('#nif').val();

            $.ajax({
                url:'core/search_im.php',
                method:"POST",
				dataType:"json",
                data:{matricule:im, ncu:ncu,verif:verif},
                success: function(response){
                    $('#vrlist').html(response.list);
					$('#nombre_verif').val(response.nombre);
                    $('#im').val('');
                }
            });
        }
	
        function add_exo_control(){
        var ncu = $('#ncu14').val();
        var exo = $('#exo').val();
        var imp = $('#imp').val();

            $.ajax({
                url:'core/add_control.php',
                method:'POST',
				dataType:"json",
                data:{action:'add_exo',ncu:ncu,exo:exo,imp:imp},
                success: function(response){
				console.log(response);
                $('#list_control').html(response.output);
				$('#verif_count').val(response.nombre);
                }
            });
        }
        //En attente d'utilisation.
        function show_ncu(data){

            var htmldata ='<div class="row"><div class="col-md-6"><h3>Dossier N° '+data.ncu+'</h3></div><div class="col-md-6"><img src="files/ncu_files/'+data.barcode+'" alt="NCU barcode"></div></div>';
            $('#ncu_area').slideDown(300,function(htmldata){
                $(this).html(htmldata);

            });                          
        }
		
	

});