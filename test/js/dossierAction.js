/**
 * Created by SIFF on 25/07/2017.
 */

$('#im').keyup(function(){
    infosIm();
});

var i=0;
$(document).on('click','#addIm',function(event){
    event.preventDefault();
    i=AddImVerif(i);
});

$(document).on('click','.imRemove',function(){
    var button_id = $(this).attr("id");
    var imb = parseInt($('#imIndice').val());
    $('#row_'+button_id).remove();
    if(imb>0){
        var nbrim = imb-1;
        $('#imIndice').val(nbrim);
    }
});

var j=0;
$(document).on('click','#addExo',function(event) {
    event.preventDefault();
    AddExoControl();
});


$(document).on('click','.exoRemove',function(){
    var button_id = $(this).attr("id");
    var nombre = parseInt($('#exoIndice').val());
    $('#data_'+button_id).remove();
    if(nombre>0){
        var nbr = nombre-1;
        $('#exoIndice').val(nbr);
    }
});

//Créer un dossier avec NCU
$(document).on('click','#creDoss',function(event) {
    event.preventDefault();
    var imIndice = parseInt($('#imIndice').val());
    var exoIndice =parseInt($('#exoIndice').val());
    var dateDeb = $('#dateDeb').val();
    var statut = true;

    if(dateDeb==null || dateDeb==''){
        statut = false;
        msg_error('Veuillez renseigner la date de début de l\'intervention!');
        //$('#classic_error').modal({backdrop:"static"});
    }

    if(imIndice==0){
        statut = false;
        msg_error('Veuillez ajouter un vérificateur à votre dossier!');
        /*
        $('#classic_msg').html('<span>Veuillez ajouter un vérificateur à votre dossier !</span>');
        $('#classic_error').modal({backdrop:"static"});
        */
    }

    if(exoIndice==0){
        statut = false;
        msg_error('Veuillez ajouter un exercice de contrôle à votre dossier!');
        /*
        $('#classic_msg').html('<span>Veuillez ajouter un exercice de contrôle à votre dossier</span>');
        $('#classic_error').modal({backdrop:"static"});
        */
    }

    if(statut == true){
        show_loader();
        creerDoss();
    }
});


//Valider une demande d'autorisation de verificaation.
$('#validateDoss').click(function(event){
    event.preventDefault();
    show_loader();
    validateDoss();
});

$('input[type=radio][name=dostype]').click(function(){

    var mode = $('input[type=radio][name=dostype]:checked').val();
    if(mode==='vsp'){
        $('#verifMode').removeClass('hide');
    }else{
        $('#verifMode').addClass('hide');
    }
});

$('#exercices').change(function(event){
    event.preventDefault();
    var nif = $('#Nif').val();
    var annee = $('#exercices').val();
    selectImpot(nif,annee,'#impExo');
});




/**********************************************************************************************************************************************************************************************************************************************/


/********************************************
 * RERIFIER LE MATRICULE D'UN VERIFICATEUR
 ********************************************/
function searchIm(){
    var matricule = $('#im').val();
    var im = matricule.replace(/ /g,'');
    var chars = im.length;
    var nif = $('#nif').val();
    if((im!=='') && (im>111111) && (chars===6)){
        //alert(im);
        $.ajax({
            url:"core/checkdata.php",
            method:"POST",
            dataType:"json",
            data:{action:'check_verif',nif:nif,im:im},
            success: function(data){
                removeloader_plus('#im-spinner');
                console.log(data);
            }
        });
    }else{
        alert('NUMERO MATRICULE INVALIDE');
        removeloader_plus('#im-spinner');
    }
}
/********************************************
 * FIN RERIFIER LE MATRICULE D'UN VERIFICATEUR
 ********************************************/



/********************************************
 * DETAILS SUR UN IM
 ********************************************/
function infosIm(){
    putloader('#im-spinner');
    var matricule = $('#im').val();
    var im = matricule.replace(/ /g,'');
    var chars = im.length;
    if(chars>=6){
        //alert(im);
        $.ajax({
            url:"dossieraction/imData",
            method:"POST",
            dataType:"json",
            data:{action:'check_verif',im:im},
            success: function(data){
                removeloader_plus('#im-spinner');
                console.log(data);
                $('#imNom').val(data.NOM+' '+data.PRENOM);
            }
        });
    }else{
        removeloader_plus('#im-spinner');
    }
}
/********************************************
 * FIN DETAILS SUR UN IM
 ********************************************/


/********************************************
 * AJOUTER UN VERIFICATEUR A UN DOSSIER
 ********************************************/
function add_im_data(){
    var im = $('#im').val().replace(/ /g,'');
    var ncu = $('#ncu14').val();
    var verif = $('#verif').val();
    //var nif = $('#nif').val();

    $.ajax({
        url:'core/search_im.php',
        method:"POST",
        dataType:"json",
        data:{matricule:im, ncu:ncu,verif:verif},
        success: function(response){
            $('#vrlist').html(response.list);
            $('#nombre_verif').val(response.nombre);
            $('#im').val('');
        }
    });
}

/********************************************
 * FIN AJOUTER UN VERIFICATEUR A UN DOSSIER
 ********************************************/



function clearForm(element){
    $(element).val('');
}

/*******************************************
 *CREER UN DOSSIER CONTROLE
 ******************************************/
function creerDoss(){
    var nif = $('#Nif').val();
    //alert(nif);
    if (nif!='' && nif>999999999) {
        //var form = $('#ncuform').serialize();
        //var form = getElementById("ncuform").serialize();
        $.ajax({
            url: base_url+'dossieraction/nouveaudossier',
            method: 'POST',
            dataType: "json",
            data: $('#ncuform').serialize(),
            success: function (data) {
                console.log(data);
                if(data.type=='success'){
                    window.location.href = base_url+data.url;
                }else{
                    send_error();
                }
                hide_loader();
            },
            error: function () {
                send_error();
            }
        });
    }
}

function AddNcu(data){
    $('#page_title').slideUp(100);
    //$('.assuj').show(400);
    $('#ncu_area').slideDown(300);
    var htmldata = '<div class="row Mgt-15"><div class="col-md-6 text-center"><h2>NCU - ' + data.ncu +
        '</h2><input id="ncu14"  type="hidden" name="ncu" value="' + data.ncu + '"></div><div class="col-md-6 text-center"><img src="files/ncu_files/' + data.barcode + '" alt="NCU barcode" style="width:100px; height:100px;"></div></div>';
    $('#ncu_area').html(htmldata);
    $('#creDoss').hide(200);
}

function showSuccessMessage(data){
    $('#SearchNif').html('<div class="alert alert-'+data.type+' Pd-20"><span class="Title_04">'+data.msg+'</span><a href="liste.php?page=date" class="btn btn-success btn-sm pull-right">Continuer</a></div>');
}


function showVerifList(datas){
    $('#verifForms').hide();
    $('#VerificateursId').html(datas.verifs);
}

function showExolist(reponse){
    $('#AddExoForm').hide();
    $('#exoList').hide();
    $('#ExercicesId').html(reponse.controls);
}


function validateDoss(){
    $.ajax({
        url:base_url+'dossieraction/validateDoss',
        method:'POST',
        dataType:"json",
        data: $('#DossValidForm').serialize(),
        success:function(data){
            console.log(data);
            if(data.statut==true) {
                location.reload(true);
            }else{
                show_error();
            }
        },
        error: function(){
            show_error();
        }
    });
}


function AddExoControl(){
    var m =parseInt($('#exoIndice').val());
    var v_exo= $('#exercices').val();
    var v_imp = $('#impExo').val();

    $('#exoList').append('<div id="data_'+m+'" class="col-md-6"><div class="col-md-4"><input type="text" class="input-sm form-control" name="verifExo[' + m + ']" value="' + v_exo + '" size="10" readonly/></div>' +
        '<div class="col-md-4"><input type="text" class="input-sm form-control" name="verifImp[' + m + ']" value="' + v_imp + '" readonly/></div>'+
        '<button id="'+m+'" class="btn btn-danger btn-sm mil-5 exoRemove">X</button></div>');

    m++;
    $('#exoIndice').val(m);
}


function AddImVerif(){
    var n =parseInt($('#imIndice').val());
    var v_nom = $('#imNom').val();
    var v_im = $('#im').val();
    
    if(v_nom!=='' && v_im!='') {
        $('#verifList').append('<div id="row_'+n+'" class="form-group"><div class="col-md-1"></div><div class="col-md-2"><input type="text" class="input-sm form-control label-sm mil-10" name="imatri[' + n + ']" value="' + v_im + '" size="10" readonly/></div>' +
            '<div class="col-md-7"><span class="input-inline"><input type="text" class="input-sm form-control" name="verifNom[' + n + ']" value="' + v_nom + '" readonly size="32"/></div></span><span id="'+n+'" class="btn btn-danger btn-sm mil-5 imRemove"><span class=""></span>X</span><div>');
        n++;
        clearForm('#im');
        clearForm('#imNom');
        $('#imIndice').val(n);
    }
}


/*******************************************
 *CREER UN DOSSIER CONTROLE
 ******************************************/
function selectImpot(nif,annee,element){
    //$(element).html('');
    if (nif!='' && nif>999999999) {
        $.ajax({
            url: base_url+'dossieraction/impotAssuj',
            method: 'POST',
            dataType: "json",
            data: {nif:nif, annee:annee},
            success: function (data) {
                console.log(data);
                var size = data.length;
                for (var i = 0; i < size; i++) {
                    $(element).append('<option value="'+data[i].GABREV+'">'+data[i].GABREV+'</option>');
                }
                //show_ncu(data);
            },
            error: function () {
                show_error();
            }
        });
    }
}

//END DOCUMENT





