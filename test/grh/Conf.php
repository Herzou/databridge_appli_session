<?php

/**
 * Created by PhpStorm.
 * User: SIFF
 * Date: 20/05/2017
 * Time: 20:43
 */
class Conf {

    static private $databases = array(
        // Le nom d'hote est infolimon a l'IUT
        // ou localhost sur votre machine
        'hostname' => '10.2.249.205',
        //'hostname' => '192.168.100.213',
		
		
        // A l'IUT, vous avez une BDD nommee comme votre login
        // Sur votre machine, vous devrez creer une BDD
        //'dbname' => 'XE',
       'dbname' => 'sigdb',
		
		
		
        // A l'IUT, c'est votre login
        // Sur votre machine, vous avez surement un compte 'root'
        'login' => 'dgigrh',
        //'login' => 'u_control',
		
		
		
        // A l'IUT, c'est votre mdp (INE par defaut)
        // Sur votre machine personelle, vous avez creez ce mdp a l'installation
        'password' => '159753'
        //'password' => 'conTroL23'
    );

    // la variable debug est un boolean
    static private $debug = True;

    static public function getLogin() {
        //en PHP l'indice d'un tableau n'est pas forcement un chiffre.
        return self::$databases['login'];
    }

    static public function getHostname() {
        //en PHP l'indice d'un tableau n'est pas forcement un chiffre.
        return self::$databases['hostname'];
    }

    static public function getDatabase() {
        //en PHP l'indice d'un tableau n'est pas forcement un chiffre.
        return self::$databases['dbname'];
    }

    static public function getPassword() {
        //en PHP l'indice d'un tableau n'est pas forcement un chiffre.
        return self::$databases['password'];
    }
        // debug
    static public function getDebug() {
        return self::$debug;
    }

}
